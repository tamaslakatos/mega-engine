
use crate::vec::vec4::Vec4;
use crate::vec::vec3::Vec3;

use std::ops::{ Add, Sub, Mul };
use std::f32::consts::PI;

#[derive(Debug, Copy, Clone)] 
pub struct Mat4 { pub arr: [[f32; 4]; 4], }

impl Mat4 {

	pub fn init_empty() -> Mat4 {

		Mat4 { arr: [
			 [0.0, 0.0, 0.0, 0.0],
			 [0.0, 0.0, 0.0, 0.0],
			 [0.0, 0.0, 0.0, 0.0],
			 [0.0, 0.0, 0.0, 0.0]
		]}
	}

	pub fn identity() -> Mat4 {

		Mat4 { arr: [
			 [1.0, 0.0, 0.0, 0.0],
			 [0.0, 1.0, 0.0, 0.0],
			 [0.0, 0.0, 1.0, 0.0],
			 [0.0, 0.0, 0.0, 1.0]
		]}
	}

	pub fn init_with_value(x: f32) -> Mat4 {

		Mat4 { arr: [
			 [x, 0.0, 0.0, 0.0],
			 [0.0, x, 0.0, 0.0],
			 [0.0, 0.0, x, 0.0],
			 [0.0, 0.0, 0.0, x]
		]}
	}

	pub fn translate(mut self, x:  f32, y: f32, z: f32) -> Mat4 {

		self.arr[0][3] = x;
		self.arr[1][3] = y;
		self.arr[2][3] = z;

		self
	}

	pub fn scale(mut self, x: f32, y:f32, z:f32) -> Mat4 {

		self.arr[0][0] = x;
		self.arr[1][1] = y;
		self.arr[2][2] = z;

		self
	}

	fn radians(degrees: f32) -> (f32) {

		degrees * PI / 180.0
	}

	pub fn rotate_x(mut self, theta: f32) -> Mat4 {

		let c = Mat4::radians(theta).cos();
		let s = Mat4::radians(theta).sin();

		self.arr[1][1] =  c;
		self.arr[1][2] =  s;
		self.arr[2][1] =  -s;
		self.arr[2][2] =  c;

		self
	}

	pub fn rotate_y(mut self, theta: f32) -> Mat4 {

		let c = Mat4::radians(theta).cos();
		let s = Mat4::radians(theta).sin();

		self.arr[0][0] =  c;
		self.arr[0][2] =  -s;
		self.arr[2][0] =  s;
		self.arr[2][2] =  c;

		self
	}

	pub fn rotate_z(mut self, theta: f32) -> Mat4 {

		let c = Mat4::radians(theta).cos();
		let s = Mat4::radians(theta).sin();

		self.arr[0][0] =  c;
		self.arr[0][1] =  s;
		self.arr[1][0] =  -s;
		self.arr[1][1] =  c;

		self
	}

	pub fn rotate(mut self, angle: f32, x: f32, y: f32, z: f32) -> Mat4 {

		let vector = Vec3::init(x, y, z).normalize();

		let c = Mat4::radians(angle).cos();
		let omc = 1.0 - c;
		let s = Mat4::radians(angle).sin();

		self.arr[0][0] = vector.x * vector.x * omc + c;
		self.arr[0][1] = vector.x * vector.y * omc - vector.z * s;
		self.arr[0][2] = vector.x * vector.z * omc + vector.y * s;
		self.arr[1][0] = vector.x * vector.y * omc + vector.z * s;
		self.arr[1][1] = vector.y * vector.y * omc + c;
		self.arr[1][2] = vector.y * vector.z * omc - vector.x * s;
		self.arr[2][0] = vector.x * vector.z * omc - vector.y * s;
		self.arr[2][1] = vector.y * vector.z * omc + vector.x * s;
		self.arr[2][2] = vector.z * vector.z * omc + c;

		self
	}

	pub fn transpose(self) -> Mat4 {

		let mut transposed_matrix = Mat4::init_empty();

		for i in 0..self.arr.len() {
			for j in 0..self.arr[i].len() {
		
				transposed_matrix.arr[i][j] = self.arr[j][i];
			}
		}

		transposed_matrix
	}

	pub fn flatten(mut self) -> [f32; 16] {

		self = self.transpose();

		let mut f32_array: [f32; 16] = [0.0; 16];
		let mut increment = 0;

		for i in 0..self.arr.len() {
			for j in 0..self.arr[i].len() {

				f32_array[increment] = self.arr[i][j];
				increment += 1;
			}
		}

		f32_array
	}
}

impl Add<Mat4> for Mat4 {

	type Output = Mat4;

	fn add(mut self, rhs: Mat4) -> Mat4 {

		for i in 0..self.arr.len() {
			for j in 0..self.arr[i].len() {
		
				self.arr[i][j] += rhs.arr[i][j];
			}
		}

		self
	}
}

impl Sub<Mat4> for Mat4 {

	type Output = Mat4;

	fn sub(mut self, rhs: Mat4) -> Mat4 {

		for i in 0..self.arr.len() {
			for j in 0..self.arr[i].len() {
		
				self.arr[i][j] -= rhs.arr[i][j];
			}
		}

		self
	}
}

impl Mul<Mat4> for Mat4 {

	type Output = Mat4;

	fn mul(mut self, rhs: Mat4) -> Mat4 {

		for i in 0..self.arr.len() {
			for j in 0..self.arr[i].len() {
				
				let mut sum = 0.0;

				for k in 0..self.arr.len() {
				
					sum += self.arr[i][k] * rhs.arr[k][j];
				}

				self.arr[i][j] = sum;
			}
		}

		self
	}
}

impl Mul<Vec4> for Mat4 {

	type Output = Vec4;

	fn mul(self, rhs: Vec4) -> Vec4 {

		let mut array: [f32; 4] = [0.0; 4];

		for i in 0..self.arr.len() {

			let mut vec4 = Vec4::init_empty();

			vec4.x = self.arr[i][0]; vec4.y = self.arr[i][1];
			vec4.z = self.arr[i][2]; vec4.w = self.arr[i][3];

			array[i] = vec4.dot(rhs);
		}

		return Vec4::init(array[0], array[1], array[2], array[3]);
	}
}

impl PartialEq for Mat4 {

	fn eq(&self, rhs: &Mat4) -> bool {

		for i in 0..self.arr.len() {

			if self.arr[i].len() != rhs.arr[i].len() { return false }	

			for j in 0..self.arr[i].len() {
		
				if self.arr[i][j] != rhs.arr[i][j] { return false }
			}
		}

		true
	}
}
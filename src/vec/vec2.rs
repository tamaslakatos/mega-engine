use crate::util::float::integer_decode;
use luajit::ffi::luaL_Reg;
use luajit::LuaObject;
use std::hash::{Hash, Hasher};
use std::ops::{Add, Mul, Sub};

#[derive(Debug, Copy, Clone)]
pub struct Vec2 {
	pub x: f32,
	pub y: f32,
}

impl Vec2 {
	pub fn init(x: f32, y: f32) -> Vec2 {
		Vec2 {
			x: x,
			y: y,
		}
	}

	pub fn init_empty() -> Vec2 {
		Vec2 {
			x: 0.0,
			y: 0.0,
		}
	}

	pub fn dot(self, rhs: Vec2) -> f32 {
		let mut sum = 0.0;

		sum += self.x * rhs.x;
		sum += self.y * rhs.y;

		sum
	}

	pub fn negate(mut self) -> Vec2 {
		self.x = -self.x;
		self.y = -self.y;

		self
	}

	pub fn length(self) -> f32 {
		let mut sum = 0.0;

		sum += self.x * self.x;
		sum += self.y * self.y;

		return sum.sqrt();
	}

	pub fn normalize(mut self) -> Vec2 {
		let len = Vec2::length(self);

		self.x /= len;
		self.y /= len;

		self
	}

	pub fn scale(mut self, s: f32) -> Vec2 {
		self.x *= s;
		self.y *= s;

		self
	}

	pub fn flatten(self) -> [f32; 2] {
		let mut f32_array: [f32; 2] = [0.0; 2];

		f32_array[0] = self.x;
		f32_array[1] = self.y;

		f32_array
	}
}

impl Add<Vec2> for Vec2 {
	type Output = Vec2;

	fn add(mut self, rhs: Vec2) -> Vec2 {
		self.x += rhs.x;
		self.y += rhs.y;

		self
	}
}

impl Sub<Vec2> for Vec2 {
	type Output = Vec2;

	fn sub(mut self, rhs: Vec2) -> Vec2 {
		self.x -= rhs.x;
		self.y -= rhs.y;

		self
	}
}

impl Mul<Vec2> for Vec2 {
	type Output = Vec2;

	fn mul(mut self, rhs: Vec2) -> Vec2 {
		self.x *= rhs.x;
		self.y *= rhs.y;

		self
	}
}

impl PartialEq for Vec2 {
	fn eq(&self, rhs: &Vec2) -> bool {
		self.x == rhs.x && self.y == rhs.y
	}
}

impl Eq for Vec2 {}

impl LuaObject for Vec2 {
	fn name() -> *const i8 {
		c_str!("Vec2")
	}

	fn lua_fns() -> Vec<luaL_Reg> {
		vec![]
	}
}

impl Hash for Vec2 {
	fn hash<H: Hasher>(&self, state: &mut H) {
		// TODO add support for decoding f32
		integer_decode(self.x as f64).hash(state);
		integer_decode(self.y as f64).hash(state);
	}
}

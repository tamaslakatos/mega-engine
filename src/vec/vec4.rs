
use crate::mat::mat4::Mat4;

use std::ops::{ Add, Sub, Mul };

#[derive(Debug, Copy, Clone)]
pub struct Vec4 {
	pub x: f32,
	pub y: f32,
	pub z: f32,
	pub w: f32,
}

impl Vec4 {

	pub fn init(x: f32, y:f32, z:f32, w:f32) -> Vec4 {

		Vec4 { x: x, y: y, z: z, w: w }
	}

	pub fn init_empty() -> Vec4 {

		Vec4 { x: 0.0, y: 0.0, z: 0.0, w: 0.0 }
	}

	pub fn dot(self, rhs: Vec4) -> f32 {

		let mut sum = 0.0;

		sum += self.x * rhs.x;
		sum += self.y * rhs.y;
		sum += self.z * rhs.z;
		sum += self.w * rhs.w;

		sum
	}

	pub fn negate(mut self) -> Vec4 {

		self.x = -self.x;
		self.y = -self.y;
		self.z = -self.z;
		self.w = -self.w;

		self
	}

	pub fn length(self) -> f32 {

		let mut sum = 0.0;

		sum += self.x * self.x;
		sum += self.y * self.y;
		sum += self.z * self.z;
		sum += self.w * self.w;

		return sum.sqrt();
	}

	pub fn normalize(mut self) -> Vec4 {

		let len = Vec4::length(self);

		self.x /= len;
		self.y /= len;
		self.z /= len;
		self.w /= len;

		self
	}

	pub fn scale(mut self, s: f32) -> Vec4 {

		self.x *= s;
		self.y *= s;
		self.z *= s;
		self.w *= s;

		self 
	}

	pub fn flatten(self) -> [f32; 4] {

		let mut f32_array: [f32; 4] = [0.0; 4];

		f32_array[0] = self.x;
		f32_array[1] = self.y;
		f32_array[2] = self.z;
		f32_array[3] = self.w;

		f32_array
	}
}

impl Add<Vec4> for Vec4 {

	type Output = Vec4;

	fn add(mut self, rhs: Vec4) -> Vec4 {

		self.x += rhs.x;
		self.y += rhs.y;
		self.z += rhs.z;
		self.w += rhs.w;

		self
	}
}

impl Sub<Vec4> for Vec4 {

	type Output = Vec4;

	fn sub(mut self, rhs: Vec4) -> Vec4 {

		self.x -= rhs.x;
		self.y -= rhs.y;
		self.z -= rhs.z;
		self.w -= rhs.w;

		self
	}
}

impl Mul<Vec4> for Vec4 {

	type Output = Vec4;

	fn mul(mut self, rhs: Vec4) -> Vec4 {

		self.x *= rhs.x;
		self.y *= rhs.y;
		self.z *= rhs.z;
		self.w *= rhs.w;

		self
	}
}

impl Mul<Mat4> for Vec4 {

	type Output = Vec4;

	fn mul(self, rhs: Mat4) -> Vec4 {

		let mut array: [f32; 4] = [0.0; 4];

		for i in 0..rhs.arr.len() {

			let mut vec4 = Vec4::init_empty();

			vec4.x = rhs.arr[i][0]; vec4.y = rhs.arr[i][1];
			vec4.z = rhs.arr[i][2]; vec4.w = rhs.arr[i][3];

			array[i] = vec4.dot(self);
		}

		return Vec4::init(array[0], array[1], array[2], array[3]);
	}
}

impl PartialEq for Vec4 {

	fn eq(&self, rhs: &Vec4) -> bool {

		self.x == rhs.x &&
		self.y == rhs.y &&
		self.z == rhs.z &&
		self.w == rhs.w
	}
}
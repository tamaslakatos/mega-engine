
use std::ops::{ Add, Sub, Mul };

#[derive(Debug, Copy, Clone)]
pub struct Vec3 { pub x: f32, pub y: f32, pub z: f32, }

impl Vec3 {

	pub fn init(x: f32, y:f32, z:f32) -> Vec3 {
		
		Vec3 { x: x, y: y, z: z }
	}

	pub fn init_empty() -> Vec3 {
		
		Vec3 { x: 0.0, y: 0.0, z: 0.0 }
	}

	pub fn dot(self, rhs: Vec3) -> f32 {

		let mut sum = 0.0;

		sum += self.x * rhs.x;
		sum += self.y * rhs.y;
		sum += self.z * rhs.z;

		sum
	}

	pub fn negate(mut self) -> Vec3 {

		self.x = -self.x;
		self.y = -self.y;
		self.z = -self.z;

		self
	}

	pub fn cross(self, rhs: Vec3) -> Vec3 {

		Vec3 {

			x: self.y * rhs.z - self.z * rhs.y,
			y: self.z * rhs.x - self.x * rhs.z,
			z: self.x * rhs.y - self.y * rhs.x
		}
	}

	pub fn length(self) -> f32 {

		let mut sum = 0.0;

		sum += self.x * self.x;
		sum += self.y * self.y;
		sum += self.z * self.z;

		return sum.sqrt();
	}

	pub fn normalize(mut self) -> Vec3 {

		let len = Vec3::length(self);

		self.x /= len;
		self.y /= len;
		self.z /= len;

		self
	}

	pub fn scale(mut self, s: f32) -> Vec3 {

		self.x *= s;
		self.y *= s;
		self.z *= s;

		self 
	}

	pub fn flatten(self) -> [f32; 3] {

		let mut f32_array: [f32; 3] = [0.0; 3];

		f32_array[0] = self.x;
		f32_array[1] = self.y;
		f32_array[2] = self.z;

		f32_array
	}
}

impl Add<Vec3> for Vec3 {

	type Output = Vec3;

	fn add(mut self, rhs: Vec3) -> Vec3 {

		self.x += rhs.x;
		self.y += rhs.y;
		self.z += rhs.z;

		self
	}
}

impl Sub<Vec3> for Vec3 {

	type Output = Vec3;

	fn sub(mut self, rhs: Vec3) -> Vec3 {

		self.x -= rhs.x;
		self.y -= rhs.y;
		self.z -= rhs.z;

		self
	}
}

impl Mul<Vec3> for Vec3 {

	type Output = Vec3;

	fn mul(mut self, rhs: Vec3) -> Vec3 {

		self.x *= rhs.x;
		self.y *= rhs.y;
		self.z *= rhs.z;

		self
	}
}

impl PartialEq for Vec3 {

	fn eq(&self, rhs: &Vec3) -> bool {

		self.x == rhs.x &&
		self.y == rhs.y &&
		self.z == rhs.z
	}
}
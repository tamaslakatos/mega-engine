#[macro_use]
extern crate luajit;

mod am;
mod callback;
mod components;
mod ecs;
mod gui;
mod mat;
mod opengl;
mod shapes;
mod systems;
mod util;
mod vec;
mod wrapper;

pub use am::actor::{self, Actor, Spawn};
pub use am::systems::{
	ActorCollisionMessage, ActorCollisionSystem, ActorRendererMessage, ActorRendererSystem,
};
pub use callback::EventHandler;
pub use components::{collider, mesh, render, transform};
pub use components::{Collider, Collision, Color, Mesh, Render, Transform};
pub use ecs::{system, Entities, GetComponent, SetComponent, System};
pub use gui::{keyboard, window, window::Window};
pub use shapes::{square, Shape, Square};
pub use systems::{CollisionSystem, RendererSystem};
pub use vec::vec2::Vec2;

use ecs::entity;
use luajit::ffi::{luaL_Reg, lua_State};
use luajit::{c_int, State};

/// this function is called by lua via require("libmega")
/// it should register all the available rust functions to a table and return it
#[no_mangle]
unsafe extern "C" fn luaopen_libmega(l: &mut lua_State) -> c_int {
	let mut state = State::from_ptr(l);
	state.settop(0);

	// add window table
	state.new_table();

	state.register_fns(
		None,
		vec![
			luaL_Reg {
				name: c_str!("entities"),
				func: Some(entity::lua::new),
			},
			lua_func!("transform", transform::lua::new),
			lua_func!("render", render::lua::new),
			lua_func!("collider", collider::lua::new),
			lua_func!("square", square::lua::new),
			luaL_Reg {
				name: c_str!("handle_events"),
				func: Some(window::lua::handle_events),
			},
			luaL_Reg {
				name: c_str!("on_keypress"),
				func: Some(window::lua::set_keypress_callback),
			},
		],
	);
	window::lua::setup_table(&mut state);
	state.set_field(1, "window");

	// add keyboard table
	state.new_table();
	keyboard::lua::setup_table(l);
	state.set_field(1, "key");

	// add mesh table
	mesh::lua::setup_table(&mut state);
	state.set_field(1, "mesh");

	// add system table
	system::lua::setup_table(&mut state);
	state.set_field(1, "system");

	actor::lua::setup_table(&mut state);
	state.set_field(1, "actor");

	// resize stack to only return mega table
	state.settop(1);

	1 // indicate something was returned
}

use luajit::ffi::luaL_Reg;
use luajit::{c_int, LuaObject};
use std::cell::RefCell;
use std::sync::{Arc, Mutex};

pub enum EventHandler {
	Rust(Arc<Mutex<RefCell<Box<FnMut() + Send + Sync>>>>),
	//Rust(&'static Fn()),
	Lua(c_int),
}

impl EventHandler {
	pub fn new<T: 'static + FnMut() + Send + Sync>(callback: T) -> Self {
		EventHandler::Rust(Arc::new(Mutex::new(RefCell::new(Box::new(callback)))))
	}
}

// function pointer's don't implement clone, but they do implement copy, so use that for cloning
// src: https://stackoverflow.com/a/34653590/1097658
impl Clone for EventHandler {
	fn clone(&self) -> Self {
		match self {
			EventHandler::Rust(a) => EventHandler::Rust(Arc::clone(a)),
			EventHandler::Lua(a) => EventHandler::Lua(*a),
		}
	}
}

impl LuaObject for EventHandler {
	fn name() -> *const i8 {
		c_str!("EventHandler")
	}

	fn lua_fns() -> Vec<luaL_Reg> {
		vec![]
	}
}

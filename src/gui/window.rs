// Wrapper over a glfw window singleton, incl. auto-initialization if necessary.
// Also maintains a hashmap of keypress event handlers which can be modified at
// runtime, as well as lua bindings.
//
// This is aimed at games using the lua FFI and/or requiring keybindings to be changed
// at runtime -- if these are static and known at compile time then hardcoding them
// will be more efficient.
use crate::callback::EventHandler;
use glfw::{self, Action, Context};
use std::borrow::BorrowMut;
use std::cell::RefCell;
use std::collections::HashMap;
use std::sync::mpsc::Receiver;
use std::sync::{Arc, Mutex};

pub struct Window {
	glfw: glfw::Glfw, // TODO move to src/opengl/glfw.rs
	inner: glfw::Window,
	events: Receiver<(f64, glfw::WindowEvent)>,
	keypress_handlers: HashMap<glfw::Key, EventHandler>,
}

// impl window as a static singleton for now
// TODO add support for multiple windows (hard since window gets dropped when returned to lua)
// TODO could probably make this thread safe by wrapping RefCell with Arc
// TODO maybe use a static vector of windows, with glfw as a separate singleton?
thread_local!(static WINDOW: Option<RefCell<Window>> = Some(RefCell::new(Window::new())));

// don't expose - called implicitly
impl Window {
	pub fn new() -> Window {
		let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
		glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
		glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));
		glfw.window_hint(glfw::WindowHint::OpenGlProfile(glfw::OpenGlProfileHint::Core));

		let (mut inner, events) = glfw
			.create_window(640, 640, "Mega Engine", glfw::WindowMode::Windowed)
			.expect("couldn't create glfw window");

		inner.set_key_polling(true);
		inner.make_current();
		glfw.set_swap_interval(glfw::SwapInterval::Sync(0));

		gl::load_with(|system| inner.get_proc_address(system));
		unsafe {
			gl::ClearColor(0.0, 0.0, 0.0, 1.0);
		}
		inner.render_context().swap_buffers();
		Window {
			glfw,
			inner,
			events,
			keypress_handlers: HashMap::new(),
		}
	}

	pub fn should_close(&self) -> bool {
		self.inner.should_close()
	}

	pub fn close(&mut self) {
		self.inner.set_should_close(true);
	}

	pub fn set_keypress_callback(
		&mut self,
		keycode: glfw::Key,
		keyhandler: Arc<Mutex<RefCell<Box<FnMut() + Send + Sync>>>>,
	) {
		self.keypress_handlers.insert(keycode, EventHandler::Rust(keyhandler));
	}

	pub fn handle_events(&mut self) {
		let mut keyhandlers = Vec::new();

		self.glfw.poll_events();
		for (_, event) in glfw::flush_messages(&self.events) {
			match event {
				glfw::WindowEvent::Key(keycode, _, Action::Press, _) => {
					if let Some(keyhandler) = self.keypress_handlers.get(&keycode) {
						keyhandlers.push(keyhandler);
					}
				}
				_ => (),
			}
		}

		for keyhandler in keyhandlers {
			let mut keyhandler = keyhandler.clone();
			if let EventHandler::Rust(callback) = keyhandler {
				let mut callback = callback.lock().unwrap();
				(&mut *(*callback).borrow_mut())();
			} else {
				panic!("ffi callback called from rust handler");
			}
		}
	}
}

pub fn init() {
	WINDOW.with(|_| ());
}

pub fn should_close() -> bool {
	WINDOW.with(|window| match window {
		Some(window) => window.borrow().should_close(),
		None => true,
	})
}

pub fn close() {
	WINDOW.with(|window| {
		if let Some(window) = window {
			window.borrow_mut().close();
		}
	});
}

pub fn set_keypress_callback(
	keycode: glfw::Key,
	keyhandler: Arc<Mutex<RefCell<Box<FnMut() + Send + Sync>>>>,
) {
	WINDOW.with(|window| {
		if let Some(window) = window {
			window.borrow_mut().set_keypress_callback(keycode, keyhandler);
		}
	});
}

pub fn handle_events() {
	WINDOW.with(|window| {
		if let Some(window) = window {
			window.borrow_mut().handle_events();
		}
	});
}

/// panics on failure
pub fn get_render_context() -> glfw::RenderContext {
	WINDOW.with(|window| {
		if let Some(window) = window {
			let mut window = window.borrow_mut();
			window.inner.render_context()
		} else {
			panic!("no window render context found")
		}
	})
}

pub fn swap_buffers() {
	WINDOW.with(|window| {
		// TODO make unsafe optimized version with unwrap() instead of if let
		if let Some(window) = window {
			let mut window = window.borrow_mut();
			window.inner.render_context().swap_buffers();
		}
	});
}

pub fn clear() {
	WINDOW.with(|window| {
		// TODO make unsafe optimized version with unwrap() instead of if let
		if let Some(window) = window {
			let mut window = window.borrow_mut();
			unsafe { gl::Clear(gl::COLOR_BUFFER_BIT) };
		}
	});
}

pub mod lua {
	use super::{EventHandler, WINDOW};
	use crate::keyboard::KeyCode;
	use glfw::{Action, Context};
	use luajit::ffi::{luaL_ref, lua_State, lua_call, lua_rawgeti, LUA_REGISTRYINDEX};
	use luajit::{c_int, State};

	/// setup a table containing the fns in this mod and push it on the lua stack
	pub fn setup_table(state: &mut State) {
		state.new_table();
		state.register_fns(
			None,
			vec![
				lua_func!("start", start),
				lua_func!("should_close", should_close),
				lua_func!("close", close),
				lua_func!("swap_buffers", swap_buffers),
			],
		);
	}

	// call from lua_openlib fn to init window if it doesn't exist and do nothing else
	// this is also done implicitly from the other fns
	pub fn start(state: &mut State) -> c_int {
		WINDOW.with(|_| {}); // initialize window if not already created
		println!("rust - window created");
		0
	}

	pub fn should_close(state: &mut State) -> c_int {
		state.settop(0);
		WINDOW.with(|window| {
			if let Some(window) = window {
				state.push(window.borrow().should_close());
			}
		});
		1
	}

	pub fn close(state: &mut State) -> c_int {
		WINDOW.with(|window| {
			if let Some(window) = window {
				window.borrow_mut().close();
			}
		});
		0
	}

	pub unsafe extern "C" fn set_keypress_callback(l: *mut lua_State) -> c_int {
		let mut state = State::from_ptr(l);
		let keycode: &KeyCode = unsafe { &*state.to_userdata(1).unwrap() };
		let callback = luaL_ref(l, LUA_REGISTRYINDEX);

		WINDOW.with(|window| {
			if let Some(window) = window {
				window
					.borrow_mut()
					.keypress_handlers
					.insert(keycode.0, EventHandler::Lua(callback));
			}
		});
		0
	}

	fn handle_event(l: *mut lua_State, callback: c_int) {
		unsafe {
			lua_rawgeti(l, LUA_REGISTRYINDEX, callback);
			lua_call(l, 0, 0);
		}
	}

	pub unsafe extern "C" fn handle_events(l: *mut lua_State) -> c_int {
		let mut keyhandlers = Vec::new();

		WINDOW.with(|window| {
			if let Some(window) = window {
				let mut window = window.borrow_mut();
				//let mut window = window.borrow_mut();
				window.glfw.poll_events();
				for (_, event) in glfw::flush_messages(&window.events) {
					match event {
						glfw::WindowEvent::Key(keycode, _, Action::Press, _) => {
							if let Some(keyhandler) = window.keypress_handlers.get(&keycode) {
								keyhandlers.push(keyhandler.clone());
							}
						}
						_ => (),
					}
				}
			}
		});

		for keyhandler in keyhandlers {
			match keyhandler {
				EventHandler::Rust(callback) => {
					let mut callback = callback.lock().unwrap();
					(&mut *(*callback).borrow_mut())();
				}
				EventHandler::Lua(callback) => handle_event(l, callback),
			}
		}
		0
	}

	pub fn swap_buffers(state: &mut State) -> c_int {
		super::swap_buffers();
		/*
		WINDOW.with(|window| {
			// TODO make unsafe optimized version with unwrap() instead of if let
			if let Some(window) = window {
				let mut window = window.borrow_mut();
				window.inner.render_context().swap_buffers();
			}
		});
		*/
		0
	}
}

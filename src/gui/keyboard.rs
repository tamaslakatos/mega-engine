// expose glfw key enums to lua via table
// TODO are these iterated at runtime? if so, use a macro instead.

use glfw::Key;

pub struct KeyCode(pub Key);

pub mod lua {
	use super::KeyCode;
	use glfw::Key;
	use luajit::ffi::{luaL_Reg, lua_State};
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for KeyCode {
		fn name() -> *const i8 {
			c_str!("KeyCode")
		}
		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}

	// TODO convert keycodes to strings with a macro
	fn add_keycodes(state: &mut State, table: c_int, kvs: Vec<(&str, Key)>) {
		for (name, value) in kvs {
			state.push(KeyCode(value));
			state.set_field(table, name);
		}
	}

	pub fn setup_table(l: &mut lua_State) {
		let mut state = State::from_ptr(l);
		state.new_table();
		let table = unsafe { luajit::ffi::lua_gettop(l) };
		add_keycodes(
			&mut state,
			table,
			vec![
				("SPACE", Key::Space),
				("APOSTROPHE", Key::Apostrophe),
				("COMMA", Key::Comma),
				("MINUS", Key::Minus),
				("PERIOD", Key::Period),
				("SLASH", Key::Slash),
				("NUM0", Key::Num0),
				("NUM1", Key::Num1),
				("NUM2", Key::Num2),
				("NUM3", Key::Num3),
				("NUM4", Key::Num4),
				("NUM5", Key::Num5),
				("NUM6", Key::Num6),
				("NUM7", Key::Num7),
				("NUM8", Key::Num8),
				("NUM9", Key::Num9),
				("SEMICOLON", Key::Semicolon),
				("EQUAL", Key::Equal),
				("A", Key::A),
				("B", Key::B),
				("C", Key::C),
				("D", Key::D),
				("E", Key::E),
				("F", Key::F),
				("G", Key::G),
				("H", Key::H),
				("I", Key::I),
				("J", Key::J),
				("K", Key::K),
				("L", Key::L),
				("M", Key::M),
				("N", Key::N),
				("O", Key::O),
				("P", Key::P),
				("Q", Key::Q),
				("R", Key::R),
				("S", Key::S),
				("T", Key::T),
				("U", Key::U),
				("V", Key::V),
				("W", Key::W),
				("X", Key::X),
				("Y", Key::Y),
				("Z", Key::Z),
				("LEFTBRACKET", Key::LeftBracket),
				("BACKSLASH", Key::Backslash),
				("RIGHTBRACKET", Key::RightBracket),
				("GRAVEACCENT", Key::GraveAccent),
				("WORLD1", Key::World1),
				("WORLD2", Key::World2),
				("ESC", Key::Escape),
				("ENTER", Key::Enter),
				("TAB", Key::Tab),
				("BACKSPACE", Key::Backspace),
				("INSERT", Key::Insert),
				("DELETE", Key::Delete),
				("RIGHT", Key::Right),
				("LEFT", Key::Left),
				("DOWN", Key::Down),
				("UP", Key::Up),
				("PAGEUP", Key::PageUp),
				("PAGEDOWN", Key::PageDown),
				("HOME", Key::Home),
				("END", Key::End),
				("CAPSLOCK", Key::CapsLock),
				("SCROLLLOCK", Key::ScrollLock),
				("NUMLOCK", Key::NumLock),
				("PRINTSCREEN", Key::PrintScreen),
				("PAUSE", Key::Pause),
				("F1", Key::F1),
				("F2", Key::F2),
				("F3", Key::F3),
				("F4", Key::F4),
				("F5", Key::F5),
				("F6", Key::F6),
				("F7", Key::F7),
				("F8", Key::F8),
				("F9", Key::F9),
				("F10", Key::F10),
				("F11", Key::F11),
				("F12", Key::F12),
				("F13", Key::F13),
				("F14", Key::F14),
				("F15", Key::F15),
				("F16", Key::F16),
				("F16", Key::F17),
				("F18", Key::F18),
				("F19", Key::F19),
				("F20", Key::F20),
				("F21", Key::F21),
				("F22", Key::F22),
				("F23", Key::F23),
				("F24", Key::F24),
				("F25", Key::F25),
				("KP0", Key::Kp0),
				("KP1", Key::Kp1),
				("KP2", Key::Kp2),
				("KP3", Key::Kp3),
				("KP4", Key::Kp4),
				("KP5", Key::Kp5),
				("KP6", Key::Kp6),
				("KP7", Key::Kp7),
				("KP8", Key::Kp8),
				("KP9", Key::Kp9),
				("KPDEC", Key::KpDecimal),
				("KPDIV", Key::KpDivide),
				("KPMUL", Key::KpMultiply),
				("KPSUB", Key::KpSubtract),
				("KPADD", Key::KpAdd),
				("KPENTER", Key::KpEnter),
				("KPEQ", Key::KpEqual),
				("LSHIFT", Key::LeftShift),
				("LCTRL", Key::LeftControl),
				("LALT", Key::LeftAlt),
				("LSUPER", Key::LeftSuper),
				("RSHIFT", Key::RightShift),
				("RCTRL", Key::RightControl),
				("RALT", Key::RightAlt),
				("RSUPER", Key::RightSuper),
				("MENU", Key::Menu),
				("UNKNOWN", Key::Unknown),
			],
		);
		state.settop(table);
	}
}

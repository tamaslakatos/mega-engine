macro_rules! impl_tuple_components {

	(($t:ident $(, $rest:ident )*)) => {

		impl<'a, $t: GetComponent<'a>, $( $rest: GetComponent<'a> ),*>GetComponent<'a> for ($t, $( $rest ),*)  {

			type Output = ($t::Output, $($rest::Output,)*);

			#[allow(non_snake_case)]

			fn get_component<'b: 'a>(entities: &'b Entities, entity: EntityId) -> Option<Self::Output> {

				$t::get_component(entities, entity).and_then(|first|<( $( $rest ,)* )>::get_component(entities, entity).map(|( $( $rest ,)* )|(first, $( $rest ),*)))
			}
		}

		impl<'a, $t: SetComponent, $( $rest: SetComponent ),*> SetComponent for ($t, $( $rest ),*) {

			#[allow(non_snake_case)]

			fn set_component(self, entities: &mut Entities, entity: EntityId) {

				let ($t, $( $rest ,)*) = self;
				$t.set_component(entities, entity);
				$($rest.set_component(entities, entity);)*
			}
		}

		impl_tuple_components! { ( $( $rest ),* ) }
	};

	(()) => {

		impl<'a> GetComponent<'a> for () {

			type Output = ();
			fn get_component<'b: 'a>(_: &'b Entities, _: EntityId) -> Option<Self> { Some(()) }
		}

		impl SetComponent for () {

			fn set_component(self, _: &mut Entities, _: EntityId) { }
		}
	};
}

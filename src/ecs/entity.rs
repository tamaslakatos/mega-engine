extern crate dynamic;

use dynamic::Dynamic;

use crate::ecs::component::{GetComponent, SetComponent};

use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::marker::Send;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EntityId(pub usize);
#[derive(Debug)]
pub struct Entities {
	pub entity: EntityId,
	pub entities: HashMap<EntityId, Components>,
}
#[derive(Debug)]
pub struct Components {
	pub components: HashMap<TypeId, Box<Dynamic>>,
}

unsafe impl Send for Entities {}
unsafe impl Send for Components {}

impl Entities {
	pub fn new() -> Entities {
		Entities {
			entity: EntityId(0),
			entities: HashMap::new(),
		}
	}

	pub fn create_entity(&mut self) -> EntityId {
		let entity = self.entity;

		debug_assert!(!self.entities.contains_key(&entity));
		self.entities.insert(entity, Components::new());

		let mut next_entity = self.entity.0 + 1;

		while self.entities.contains_key(&EntityId(next_entity)) {
			next_entity += 1
		}
		self.entity = EntityId(next_entity);

		entity
	}

	pub fn remove_entity(&mut self, entity: EntityId) {
		self.entities.remove(&entity);
		self.entity = entity;
	}

	pub fn get_component<'a, T: Any>(&self, entity: EntityId) -> Option<&T> {
		self.entities.get(&entity).and_then(|component| component.get::<T>())
	}

	pub fn get_entity_with_components<'a, 'b: 'a, T: GetComponent<'a>>(&'b self, entity: EntityId) -> Option<(EntityId, T::Output)> {
		self.entities.get(&entity).and_then(|components| T::get_component(&self, entity).map(|component| (entity, component)))
	}

	pub fn get_mut_component<'a, T: Any>(&mut self, entity: EntityId) -> Option<&mut T> {
		self.entities.get_mut(&entity).and_then(|component| component.get_mut::<T>())
	}

	pub fn get_entities_with_components<'a, 'b: 'a, T: GetComponent<'a>>(
		&'b self,
	) -> Vec<(EntityId, T::Output)> {
		self.entities
			.keys()
			.filter_map(|&entity| {
				T::get_component(&self, entity).map(|component| (entity, component))
			})
			.collect::<Vec<_>>()
	}

	pub fn set_component<T: Any + SetComponent>(&mut self, entity: EntityId, component: T) {
		if let Some(entity) = self.entities.get_mut(&entity) {
			entity.insert(component);
		}
	}
}

impl Components {
	pub fn new() -> Components {
		Components {
			components: HashMap::new(),
		}
	}

	pub fn get<T: Any>(&self) -> Option<&T> {
		self.components.get(&TypeId::of::<T>()).and_then(|component| component.downcast_ref::<T>())
	}

	pub fn get_mut<T: Any>(&mut self) -> Option<&mut T> {
		self.components
			.get_mut(&TypeId::of::<T>())
			.and_then(|component| component.downcast_mut::<T>())
	}

	pub fn insert<T: Any>(&mut self, component: T) {
		self.components.insert(TypeId::of::<T>(), Dynamic::new(component));
	}
}

pub mod lua {
	use super::{Any, Components, Entities, EntityId, SetComponent};
	//use crate::wrapper::cell::Cell;
	use crate::{Collider, Color, Mesh, Render, Shape, Square, Transform};
	use luajit::ffi::{luaL_Reg, lua_State};
	use luajit::{c_int, LuaObject, State};
	use std::convert::TryFrom;

	// entities

	impl LuaObject for EntityId {
		fn name() -> *const i8 {
			c_str!("EntityId")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}

	impl LuaObject for Entities {
		fn name() -> *const i8 {
			c_str!("Entities")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![
				luaL_Reg {
					name: c_str!("create_entity"),
					func: Some(lua_create_entity),
				},
				lua_method!("debug_print", Entities, Entities::lua_debug_print),
				lua_method!("set_collider", Entities, Entities::lua_set_component<Collider>),
				lua_method!("set_square_mesh", Entities, Entities::lua_set_mesh_component<Square>),
				lua_method!("set_render", Entities, Entities::lua_set_component<Render>),
				lua_method!("get_transform", Entities, Entities::lua_get_component<Transform>),
				lua_method!("set_transform", Entities, Entities::lua_set_component<Transform>),
			]
		}
	}

	pub unsafe extern "C" fn new(l: *mut lua_State) -> c_int {
		let mut state = State::from_ptr(l);
		state.settop(0);
		let entities = Entities::new();
		state.push(entities); // TODO this drops entities :(
		1
	}

	pub unsafe extern "C" fn lua_create_entity(l: *mut lua_State) -> c_int {
		let mut state = State::from_ptr(l);
		let entities: &mut Entities = unsafe { &mut *state.to_userdata(1).unwrap() };
		// TODO might overflow
		let id = entities.create_entity();
		//let id = i32::try_from(entities.create_entity().0).ok().unwrap();
		state.settop(0);
		// TODO check bounds when casting
		state.push(i32::try_from(id.0).ok().unwrap());
		1
	}

	impl Entities {
		pub fn lua_get_component<T: Any + LuaObject + Copy>(&mut self, state: &mut State) -> c_int {
			// TODO check bounds when casting
			let id = state.to_int(2).unwrap();
			let id = usize::try_from(id).ok().unwrap();
			let id = EntityId(id);
			state.settop(0);
			match self.get_component::<T>(id) {
				Some(component) => state.push(*component),
				None => state.push_nil(),
			};
			1
		}

		pub fn lua_set_component<T: Any + SetComponent + Copy>(
			&mut self,
			state: &mut State,
		) -> c_int {
			// TODO check bounds when casting
			//let entity: &Entity = unsafe { &*state.to_userdata(2).unwrap() };
			let id = state.to_int(2).unwrap();
			let id = usize::try_from(id).ok().unwrap();
			let id = EntityId(id);
			let component: T = unsafe { *state.to_userdata(3).unwrap() };
			self.set_component(id, component);
			0
		}

		// lua_set_component doesn't work with mesh since the size of Shape is not known
		// at compile time, so this function handles meshes instead.
		pub fn lua_set_mesh_component<T: 'static + Shape + Copy>(
			&mut self,
			state: &mut State,
		) -> c_int {
			let id = EntityId(usize::try_from(state.to_int(2).unwrap()).ok().unwrap());
			let shape: T = unsafe { *state.to_userdata(3).unwrap() };
			state.get_field(4, "r");
			state.get_field(4, "g");
			state.get_field(4, "b");
			let color = Color::new(
				state.to_int(-3).unwrap() as u8,
				state.to_int(-2).unwrap() as u8,
				state.to_int(-1).unwrap() as u8,
			);
			self.set_component(id, Mesh::new(shape, color));
			0
		}

		pub fn lua_debug_print(&self, state: &State) -> c_int {
			println!("{:#?}", self);
			0
		}
	}

	// components

	impl LuaObject for Components {
		fn name() -> *const i8 {
			c_str!("Components")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![
				lua_method!("get", Components, Components::lua_get),
				lua_method!("insert", Components, Components::lua_insert),
			]
		}
	}

	pub unsafe extern "C" fn new_components(l: *mut lua_State) -> c_int {
		let mut state = State::from_ptr(l);
		state.settop(0);
		state.push(Components::new());
		1
	}

	impl Components {
		pub fn lua_get(&mut self, state: &mut State) -> c_int {
			// TODO make a separate getter per component type,
			// and use it to call this with the appropriate
			// type id, then use that to index into the hashmap
			// in self and return the result. Finally return 1.
			0
		}

		pub fn lua_insert(&mut self, state: &mut State) -> c_int {
			// TODO setcomponent should be on stack[2].
			// retrieve it and call insert on it along with its id.
			0
		}
	}
}

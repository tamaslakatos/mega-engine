use crate::ecs::component::{GetComponent, SetComponent};
use crate::ecs::entity::EntityId;

pub trait System {
	type Input: for<'b> GetComponent<'b>;
	type Output: SetComponent;

	fn update<'b>(&mut self, entities: &[(EntityId, <Self::Input as GetComponent<'b>>::Output)]);
}

pub mod lua {
	use crate::systems;
	use luajit::ffi::luaL_Reg;
	use luajit::State;

	pub fn setup_table(state: &mut State) {
		state.new_table();
		state.register_fns(
			None,
			vec![
				luaL_Reg {
					name: c_str!("collision"),
					func: Some(systems::collision::lua::new),
				},
				lua_func!("renderer", systems::renderer::lua::new),
			],
		);
	}
}

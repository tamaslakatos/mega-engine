
use crate::ecs::entity::{Entities, EntityId};
use std::any::Any;
use std::marker::Sync;
use std::marker::Send;

pub trait GetComponent<'a>: Sized {

	type Output: Sync + Send;

	fn get_component<'b: 'a>(entities: &'b Entities, entity: EntityId) -> Option<Self::Output>;
}

pub trait SetComponent: Sized + Any {
	fn set_component(self, entities: &mut Entities, entity: EntityId) {
		entities.set_component(entity, self);
	}
}

impl<'a, T: GetComponent<'a>> GetComponent<'a> for Option<T> {
	type Output = Option<T::Output>;

	fn get_component<'b: 'a>(entities: &'b Entities, entity: EntityId) -> Option<Self::Output> {
		Some(T::get_component(entities, entity))
	}
}

impl<'a, 'any, T: Sized + Any + Sync + Send> GetComponent<'a> for &'any T {

	type Output = &'a T;

	fn get_component<'b: 'a>(entities: &'b Entities, entity: EntityId) -> Option<Self::Output> {
		entities.get_component::<T>(entity)
	}
}

impl<T: SetComponent> SetComponent for Option<T> {
	fn set_component(self, entities: &mut Entities, entity: EntityId) {
		if let Some(component) = self {
			component.set_component(entities, entity);
		}
	}
}

impl_tuple_components! { (A, B, C, D, E, F, G, H, I, J, K) }

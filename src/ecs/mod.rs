#[macro_use]
pub mod macros;

pub mod component;
pub mod entity;
pub mod system;

pub use entity::Entities;
pub use entity::EntityId;
pub use system::System;
pub use component::{ GetComponent,SetComponent };

use std::fmt::Debug;
use std::marker::Sync;
use std::marker::Send;

pub trait Shape: Debug + Sync + Send {
	fn vert(&self) -> &[f32];
}
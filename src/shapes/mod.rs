
pub mod shape;
pub mod square;

pub use shape::Shape;
pub use square::Square;
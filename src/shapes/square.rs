use crate::shapes::shape::Shape;

use std::any::TypeId;
use std::fmt::{Debug, Formatter, Result};

#[derive(Clone, Copy)]
pub struct Square {
	pub type_id: TypeId,
	pub vert: [f32; 12],
}

impl Shape for Square {
	fn vert(&self) -> &[f32] {
		&self.vert
	}
}

impl Square {
	pub fn new() -> Self {
		let vert: [f32; 12] = [-0.5, 0.5, 0.5, 0.5, 0.5, -0.5, 0.5, -0.5, -0.5, -0.5, -0.5, 0.5];

		Square {
			type_id: TypeId::of::<Square>(),
			vert: vert,
		}
	}
}

impl Debug for Square {
	fn fmt(&self, f: &mut Formatter) -> Result {
		write!(f, " typeId: {:?}", self.type_id);
		self.vert[..].fmt(f)
	}
}

pub mod lua {
	use super::Square;
	use luajit::ffi::luaL_Reg;
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for Square {
		fn name() -> *const i8 {
			c_str!("Square")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}

	pub fn new(state: &mut State) -> c_int {
		state.settop(0);
		state.push(Square::new());
		1
	}
}

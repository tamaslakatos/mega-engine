// wrapper around subset of Cell which makes it possible to impl traits on it

pub struct Cell<T> {
	pub cell: std::cell::Cell<T>,
}

impl<T> Cell<T> {
	pub fn new(value: T) -> Cell<T> {
		Self {
			cell: std::cell::Cell::new(value),
		}
	}
}

impl<T: Copy> Cell<T> {
	pub fn get(&self) -> T {
		self.cell.get()
	}
}

use crate::ecs::component::{GetComponent, SetComponent};
use crate::ecs::entity::EntityId;

use std::borrow::BorrowMut;
use std::fmt::Debug;
use std::marker::Sync;
use std::sync::mpsc::channel;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::sync::{Arc, Mutex};
use std::thread;
use std::thread::JoinHandle;

pub enum Message<T: 'static + Actor + Send> {
	Info(T::Message),
	Stop(bool),
	Ping, // used for syncing
}

pub trait Actor {
	//type Message: Send + SetComponent;
	type Message: Send;
	fn new() -> Self;
	fn recv(&mut self, msg: Self::Message);
}

#[derive(Debug)]
pub struct Spawn<T: 'static + Actor + Send> {
	pub tx_info: Sender<Message<T>>,
	pub rx_sync: Receiver<()>,
	pub handle: JoinHandle<()>,
}

/*
impl<T: 'static + Actor + Send> Drop for Spawn<T> {
	fn drop(&mut self) {
		println!("spawn dropped!");
	}
}
*/

impl<T: 'static + Actor + Send> Spawn<T> {
	/// call this from outside the actor's thread to kill it and wait for it to finish.
	pub fn join_borrowed(&mut self) {
		self.tx_info.send(Message::Stop(true));
		self.rx_sync.recv();
	}

	pub fn spawn_with_handle(contained: Arc<Mutex<T>>) -> Spawn<T> {
		let (tx_info, rx_info) = channel();
		let (tx_sync, rx_sync) = channel();
		let mut contained = Arc::clone(&contained);
		let handle = thread::spawn(move || loop {
			match rx_info.recv() {
				Ok(result) => match (result) {
					Message::Info(message) => contained.lock().unwrap().recv(message),
					Message::Stop(ack) => {
						if ack {
							tx_sync.send(()); // notify when done
						}
						break;
					}
					Message::Ping => {
						tx_sync.send(());
					}
				},
				Err(_) => break,
			}
		});

		Spawn {
			tx_info,
			rx_sync,
			handle,
		}
	}
}

pub mod lua {
	use super::{Actor, Message, Spawn};
	use crate::{ActorCollisionSystem, ActorRendererSystem};
	use luajit::ffi::{luaL_Reg, lua_State};
	use luajit::{c_int, LuaObject, State};
	use std::sync::{Arc, Mutex};

	pub fn setup_table(state: &mut State) {
		state.new_table();
		state.register_fns(
			None,
			vec![
				lua_func!("collision", lua_new<ActorCollisionSystem>),
				lua_func!("rendering", lua_new<ActorRendererSystem>),
			],
		);
	}

	impl<T: 'static + Actor + Send + LuaObject> LuaObject for Spawn<T> {
		fn name() -> *const i8 {
			T::name()
		}
		fn lua_fns() -> Vec<luaL_Reg> {
			T::lua_fns()
		}
	}

	// TODO register this when setting up lua lib
	pub fn lua_new<T: 'static + Actor + Send + LuaObject>(state: &mut State) -> c_int {
		state.settop(0);
		let spawn = Spawn::spawn_with_handle(Arc::new(Mutex::new(T::new())));
		println!("spawn created");
		state.push(spawn); // FIXME gets dropped & segfaults :(
				   /*
				   unsafe {
					   *state.new_userdata() = Spawn::spawn_with_handle(Arc::new(Mutex::new(T::new())));
				   }
				   */
		println!("spawn pushed");
		1
	}

	// TODO register this as a method on each actor
	// NOTE callers are responsible for not using spawn obj after this
	pub fn destroy<T: 'static + Actor + Send + LuaObject>(state: &mut State) -> c_int {
		let spawn: &mut Spawn<T> = unsafe { &mut *state.to_userdata(1).unwrap() };
		// we can't join the thread since it's borrowed, so we have to use this hack :(
		spawn.join_borrowed();
		0
	}
}

use crate::am::Actor;
use crate::components::{Camera, Mesh, Render, Transform};
use crate::ecs::component::SetComponent;
use crate::ecs::entity::EntityId;
use crate::gui::window;
use crate::mat::mat4::Mat4;
use crate::opengl::VAO;
use crate::vec::vec3::Vec3;

use glfw::{self, Context};
use std::ffi::CString;

pub struct ActorRendererSystem {
	pub render_context: glfw::RenderContext,
}

pub enum ActorRendererMessage {
	Render(Transform, Render, Mesh),
	RefRender(&'static Transform, &'static Render, &'static Mesh),
}

impl SetComponent for ActorRendererMessage {}

impl Actor for ActorRendererSystem {
	type Message = ActorRendererMessage;

	fn new() -> Self {
		ActorRendererSystem {
			render_context: window::get_render_context(),
		}
	}

	fn recv(&mut self, msg: ActorRendererMessage) {
		match msg {
			ActorRendererMessage::Render(transform, render, mesh) => {
				self.render(&transform, &render, &mesh)
			}
			ActorRendererMessage::RefRender(&transform, &render, &mesh) => {
				self.render(&transform, &render, &mesh)
			}
		}
	}
}

impl ActorRendererSystem {
	fn render(&mut self, transform: &Transform, render: &Render, mesh: &Mesh) {
		self.render_context.make_current();

		let projection_matrix = Camera::ortho(-0.5, 9.5, -0.5, 9.5, 1.0, 10.0);
		let view_matrix = Camera::look_at(
			Vec3::init(0.0, 0.0, 1.0),
			Vec3::init(0.0, 0.0, 0.0),
			Vec3::init(0.0, 1.0, 0.0),
		);

		let model_matrix = Mat4::identity().translate(transform.x, transform.y, 0.0);

		unsafe {
			let uniform_model_location = gl::GetUniformLocation(
				render.program.id,
				CString::new("uniform_model").unwrap().as_ptr(),
			);
			let uniform_view_location = gl::GetUniformLocation(
				render.program.id,
				CString::new("uniform_view").unwrap().as_ptr(),
			);
			let uniform_projection_location = gl::GetUniformLocation(
				render.program.id,
				CString::new("uniform_projection").unwrap().as_ptr(),
			);
			let uniform_color_location = gl::GetUniformLocation(
				render.program.id,
				CString::new("uniform_color").unwrap().as_ptr(),
			);

			gl::UniformMatrix4fv(
				uniform_model_location,
				1,
				gl::FALSE,
				model_matrix.flatten().as_ptr(),
			);
			gl::UniformMatrix4fv(
				uniform_view_location,
				1,
				gl::FALSE,
				view_matrix.flatten().as_ptr(),
			);
			gl::UniformMatrix4fv(
				uniform_projection_location,
				1,
				gl::FALSE,
				projection_matrix.flatten().as_ptr(),
			);
			gl::Uniform3f(
				uniform_color_location,
				mesh.color.r as f32 / 255.0,
				mesh.color.g as f32 / 255.0,
				mesh.color.b as f32 / 255.0,
			);
		}

		unsafe { gl::UseProgram(render.program.id) };
		unsafe { gl::BindVertexArray(VAO::new(mesh.vbo)) };
		unsafe { gl::DrawArrays(gl::TRIANGLES, 0, 6) };
	}
}

pub mod lua {
	use super::{ActorRendererMessage, ActorRendererSystem};
	use crate::am::actor;
	use crate::{Mesh, Render, Spawn, Transform};
	use luajit::ffi::{luaL_Reg, lua_State};
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for ActorRendererSystem {
		fn name() -> *const i8 {
			c_str!("RendererActor")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![
				luaL_Reg {
					name: c_str!("render"),
					func: Some(Self::lua_render),
				},
				lua_func!("kill", actor::lua::destroy::<ActorRendererSystem>),
			]
		}
	}

	impl ActorRendererSystem {
		pub unsafe extern "C" fn lua_render(l: *mut lua_State) -> c_int {
			let mut state = State::from_ptr(l);
			let spawn: &Spawn<Self> = { &*state.to_userdata(1).unwrap() };
			let transform: &Transform = { &*state.to_userdata(2).unwrap() };
			let render: &Render = { &*state.to_userdata(3).unwrap() };
			let mesh: &Mesh = { &*state.to_userdata(4).unwrap() };
			spawn.tx_info.send(actor::Message::Info(ActorRendererMessage::RefRender(
				transform, render, mesh,
			)));
			0
		}
	}
}

use crate::callback::EventHandler;
use crate::components::{Collider, Collision, Mesh, Transform};
use crate::ecs::component::SetComponent;
use crate::ecs::entity::EntityId;
use crate::vec::vec2::Vec2;
use crate::Actor;

use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::sync::Arc;

pub struct ActorCollisionSystem {
	// TODO: use Collider instead of raw hash values - currently results in lifetime problems
	callbacks: HashMap<u64, EventHandler>,
}

pub enum ActorCollisionMessage {
	Add((Collider, Collider), EventHandler),
	Check((Transform, Collider, Mesh, Transform, Collider, Mesh)),
}

impl Clone for ActorCollisionMessage {
	fn clone(&self) -> Self {
		match self {
			ActorCollisionMessage::Add(colliders, callback) => {
				ActorCollisionMessage::Add(*colliders, callback.clone())
			}
			ActorCollisionMessage::Check(components) => ActorCollisionMessage::Check(*components),
		}
	}
}
//impl SetComponent for ActorCollisionMessage {}

impl Actor for ActorCollisionSystem {
	type Message = ActorCollisionMessage;

	fn new() -> Self {
		Self {
			callbacks: HashMap::new(),
		}
	}

	fn recv(&mut self, msg: ActorCollisionMessage) {
		match msg {
			ActorCollisionMessage::Add(colliders, callback) => {
				self.add_collision_callback(colliders, callback);
			}
			ActorCollisionMessage::Check(components) => {
				self.handle_possible_collision(components);
			}
		}
	}
}
impl ActorCollisionSystem {
	fn add_collision_callback(&mut self, colliders: (Collider, Collider), callback: EventHandler) {
		// TODO use collider as key instead of using its raw hash
		let collision = Collision::new(&colliders.0, &colliders.1);
		let collision_hash = {
			let mut hasher = DefaultHasher::new();
			collision.hash(&mut hasher);
			hasher.finish()
		};
		self.callbacks.insert(collision_hash, callback);
	}

	fn handle_possible_collision(
		&mut self,
		components: (Transform, Collider, Mesh, Transform, Collider, Mesh),
	) {
		let (transform_a, collider_a, mesh_a, transform_b, collider_b, mesh_b) = components;
		let collider_a = collider_a.set(
			collider_a.id,
			Vec2::init(mesh_a.vert_min.x + transform_a.x, mesh_a.vert_min.y + transform_a.y),
			Vec2::init(mesh_a.vert_max.x + transform_a.x, mesh_a.vert_max.y + transform_a.y),
		);
		let collider_b = collider_b.set(
			collider_b.id,
			Vec2::init(mesh_b.vert_min.x + transform_b.x, mesh_b.vert_min.y + transform_b.y),
			Vec2::init(mesh_b.vert_max.x + transform_b.x, mesh_b.vert_max.y + transform_b.y),
		);
		if Self::intersects(&transform_a, &collider_a, &transform_b, &collider_b) {
			// TODO use collider as key instead of using its raw hash
			let collision = Collision::new(&collider_a, &collider_b);
			let collision_hash = {
				let mut hasher = DefaultHasher::new();
				collision.hash(&mut hasher);
				hasher.finish()
			};

			if let Some(callback) = self.callbacks.get(&collision_hash) {
				match callback {
					EventHandler::Rust(callback) => {
						let mut callback = callback.lock().unwrap();
						(&mut *(*callback).borrow_mut())();
					}
					EventHandler::Lua(ref callback) => (), // TODO
				}
			}
		}
	}

	fn intersects(
		transform_a: &Transform,
		collider_a: &Collider,
		transform_b: &Transform,
		collider_b: &Collider,
	) -> bool {
		let min_a = Vec2::init(collider_a.min.x + transform_a.x, collider_a.min.y + transform_a.y);
		let max_a = Vec2::init(collider_a.max.x + transform_a.x, collider_a.max.y + transform_a.y);
		let min_b = Vec2::init(collider_b.min.x + transform_b.x, collider_b.min.y + transform_b.y);
		let max_b = Vec2::init(collider_b.max.x + transform_b.x, collider_b.max.y + transform_b.y);

		let x_hit = collider_a.min.x <= collider_b.max.x && collider_a.max.x >= collider_b.min.x;
		let y_hit = collider_a.min.y <= collider_b.max.y && collider_a.max.y >= collider_b.min.y;
		x_hit && y_hit
	}
}

pub mod lua {
	use super::ActorCollisionSystem;
	use crate::am::actor;
	use crate::EventHandler;
	use crate::{Collider, Mesh, Spawn, Transform};
	//use luajit::ffi::{luaL_Reg, luaL_ref, lua_State};
	use luajit::ffi::{luaL_Reg, luaL_ref, lua_State, lua_call, lua_rawgeti, LUA_REGISTRYINDEX};
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for ActorCollisionSystem {
		fn name() -> *const i8 {
			c_str!("CollisionActor")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			// TODO Self::add_colliser
			// TODO Self::check_collision
			vec![lua_func!("kill", actor::lua::destroy::<ActorCollisionSystem>)]
		}
	}

	// spawn collision actor in new thread
	impl ActorCollisionSystem {
		// add collision callback
		pub unsafe extern "C" fn lua_on_inside(l: *mut lua_State) -> c_int {
			let mut state = State::from_ptr(l);
			let actor: &mut Self = { &mut *state.to_userdata(1).unwrap() };
			let a: &Collider = { &*state.to_userdata(2).unwrap() };
			let b: &Collider = { &*state.to_userdata(3).unwrap() };
			let callback = luaL_ref(l, LUA_REGISTRYINDEX);
			let handler = EventHandler::Lua(callback);

			0
		}

		// check + handle a collision
		pub fn tick(state: State) -> c_int {
			0
		}
	}
}

use crate::callback::EventHandler;
use crate::components::{Collider, Collision, Mesh, Transform};
use crate::ecs::entity::EntityId;
use crate::ecs::system::System;
use crate::vec::vec2::Vec2;
use std::cell::RefCell;

use std::collections::HashMap;

pub struct CollisionSystem<'a> {
	// TODO use hashmap over tuple of 2 Colliders
	pub inside_handlers: HashMap<Collision<'a>, EventHandler>,
}

impl<'a> System for CollisionSystem<'a> {
	type Input = (&'static Transform, &'static Collider, &'static Mesh);
	type Output = (Transform, Collider, Mesh);

	fn update(&mut self, entities: &[(EntityId, (&Transform, &Collider, &Mesh))]) {
		for mut event_handler in self.update_get_event_handlers(entities) {
			if let EventHandler::Rust(ref mut callback) = event_handler {
				let mut callback = callback.lock().unwrap();
				(&mut *(*callback).borrow_mut())();
			//(&mut (*callback.lock().unwrap()).borrow_mut())();
			} else {
				panic!("ffi callback called from rust handler")
			}
		}
	}
}

impl<'a> CollisionSystem<'a> {
	pub fn new() -> CollisionSystem<'a> {
		CollisionSystem {
			inside_handlers: HashMap::new(),
		}
	}

	fn get_event_handler(
		hashmap: &'a HashMap<Collision, EventHandler>,
		a: &'a Collider,
		b: &'a Collider,
	) -> Option<EventHandler> {
		if let Some(event_handler) = hashmap.get(&Collision::new(a, b)) {
			Some(event_handler.clone())
		} else {
			None
		}
	}

	fn update_get_event_handlers(
		&mut self,
		entities: &[(EntityId, (&Transform, &Collider, &Mesh))],
	) -> Vec<EventHandler> {
		let mut event_handlers = Vec::new();

		entities
			.into_iter()
			.map(|entity_lhs| {
				let (transform_lhs, collider_lhs, mesh_lhs) = entity_lhs.1;

				let collider_lhs = &collider_lhs.set(
					collider_lhs.id,
					Vec2::init(
						transform_lhs.x + mesh_lhs.vert_min.x,
						transform_lhs.y + mesh_lhs.vert_min.y,
					),
					Vec2::init(
						transform_lhs.x + mesh_lhs.vert_max.x,
						transform_lhs.y + mesh_lhs.vert_max.y,
					),
				);

				entities
					.into_iter()
					.filter(|entity| entity.0 != entity_lhs.0)
					.map(|entity_rhs| {
						let (transform_rhs, collider_rhs, mesh_rhs) = entity_rhs.1;

						let collider_rhs = collider_rhs.set(
							collider_rhs.id,
							Vec2::init(
								transform_rhs.x + mesh_rhs.vert_min.x,
								transform_rhs.y + mesh_rhs.vert_min.y,
							),
							Vec2::init(
								transform_rhs.x + mesh_rhs.vert_max.x,
								transform_rhs.y + mesh_rhs.vert_max.y,
							),
						);

						if CollisionSystem::intersects(&collider_lhs, &collider_rhs) {
							if let Some(inside_handler) = Self::get_event_handler(
								&self.inside_handlers,
								&collider_lhs,
								&collider_rhs,
							) {
								event_handlers.push(inside_handler);
							}
						}
					})
					.collect::<Vec<_>>();
			})
			.collect::<Vec<_>>();
		event_handlers
	}

	fn intersects(collider_lhs: &Collider, collider_rhs: &Collider) -> bool {
		let x_hit =
			collider_lhs.min.x <= collider_rhs.max.x && collider_lhs.max.x >= collider_rhs.min.x;
		let y_hit =
			collider_lhs.min.y <= collider_rhs.max.y && collider_lhs.max.y >= collider_rhs.min.y;
		x_hit && y_hit
	}
}

pub mod lua {
	use super::CollisionSystem;
	use crate::{Collider, Collision, Entities, EventHandler, Mesh, Transform};
	use luajit::ffi::{luaL_Reg, luaL_ref, lua_State, lua_call, lua_rawgeti, LUA_REGISTRYINDEX};
	use luajit::{c_int, LuaObject, State};
	use std::borrow::BorrowMut;

	impl<'a> LuaObject for CollisionSystem<'a> {
		fn name() -> *const i8 {
			c_str!("CollisionSystem")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![
				luaL_Reg {
					name: c_str!("update"),
					func: Some(CollisionSystem::lua_update),
				},
				luaL_Reg {
					name: c_str!("on_inside"),
					func: Some(CollisionSystem::lua_on_inside),
				},
			]
		}
	}

	pub unsafe extern "C" fn new(l: *mut lua_State) -> c_int {
		let mut state = State::from_ptr(l);
		state.settop(0);
		let collision = CollisionSystem::new();
		state.push(collision); // TODO why does this crash?
		1
	}

	impl<'a> CollisionSystem<'a> {
		pub unsafe extern "C" fn lua_update(l: *mut lua_State) -> c_int {
			let mut state = State::from_ptr(l);
			let system: &mut CollisionSystem = &mut *state.to_userdata(1).unwrap();
			let entities: &Entities = &*state.to_userdata(2).unwrap();
			let entities = &entities
				.get_entities_with_components::<(&'static Transform, &'static Collider, &'static Mesh)>(
				);

			for mut event_handler in system.update_get_event_handlers(entities) {
				match event_handler {
					EventHandler::Rust(callback) => {
						let mut callback = callback.lock().unwrap();
						(&mut *(*callback).borrow_mut())();
					}
					EventHandler::Lua(callback) => Self::handle_event(l, callback),
				}
			}
			0
		}

		// TODO this is duplicated from src/gui/window.rs. Move it into event_handler.rs
		fn handle_event(l: *mut lua_State, callback: c_int) {
			unsafe {
				lua_rawgeti(l, LUA_REGISTRYINDEX, callback);
				lua_call(l, 0, 0);
			}
		}

		pub unsafe extern "C" fn lua_on_inside(l: *mut lua_State) -> c_int {
			let mut state = State::from_ptr(l);
			let system: &mut CollisionSystem = { &mut *state.to_userdata(1).unwrap() };
			let a: &Collider = { &*state.to_userdata(2).unwrap() };
			let b: &Collider = { &*state.to_userdata(3).unwrap() };
			let callback = luaL_ref(l, LUA_REGISTRYINDEX);
			let handler = EventHandler::Lua(callback);
			let collision = Collision::new(a, b);
			system.inside_handlers.insert(collision, handler);
			0
		}
	}
}

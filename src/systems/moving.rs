use crate::components::{Transform, Velocity};
use crate::ecs::entity::EntityId;
use crate::ecs::system::MutSystem;

#[derive(Debug)]
pub struct Moving;

impl MutSystem for Moving {
	type Input = (&'static Transform, &'static Velocity);
	type Output = Transform; // TODO don't need velocity here

	fn update<'b>(&mut self, entities: &mut [(EntityId, (&Transform, &Velocity))]) {
		entities
			.iter_mut()
			.map(|entity| {
				let id = &entity.0;
				let components = &entity.1;
				let transform = &components.0;
				let velocity = &components.1;
				entities.set_component(
					id,
					Transform::new(transform.x + velocity.x, transform.y + velocity.y),
				);
			})
			.collect::<Vec<_>>();
	}
}

pub mod lua {
	use super::Moving;
	use crate::{Entities, MutSystem, Transform, Velocity};
	use luajit::ffi::luaL_Reg;
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for Moving {
		fn name() -> *const i8 {
			c_str!("Moving")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![lua_method!("update", Moving, Moving::lua_update)]
		}
	}

	pub fn new(state: &mut State) -> c_int {
		state.settop(0);
		state.push(Moving {});
		1
	}

	impl Moving {
		fn lua_update(&self, state: &mut State) -> c_int {
			let mut entities: &mut Entities = unsafe { &mut *state.to_userdata(2).unwrap() };
			//println!("printing entities hashmap");
			//println!("{:#?}", entities);
			let entities = &mut entities
				.get_entities_with_components::<(&'static Transform, &'static Velocity)>();
			self.update(entities);
			0
		}
	}
}

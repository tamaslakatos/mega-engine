use crate::components::{Camera, Mesh, Render, Transform};
use crate::ecs::entity::EntityId;
use crate::ecs::system::System;
use crate::mat::mat4::Mat4;
use crate::opengl::VAO;
use crate::vec::vec3::Vec3;

use std::ffi::CString;

pub struct RendererSystem;

impl System for RendererSystem {
	type Input = (&'static Transform, &'static Render, &'static Mesh);
	type Output = (Transform, Render, Mesh);

	fn update<'b>(&mut self, entities: &[(EntityId, (&Transform, &Render, &Mesh))]) {
		let projection_matrix = Camera::ortho(-0.5, 9.5, -0.5, 9.5, 1.0, 10.0);
		let view_matrix = Camera::look_at(
			Vec3::init(0.0, 0.0, 1.0),
			Vec3::init(0.0, 0.0, 0.0),
			Vec3::init(0.0, 1.0, 0.0),
		);

		unsafe {
			gl::Clear(gl::COLOR_BUFFER_BIT);
		}
		unsafe {
			gl::ClearColor(0.0, 0.0, 0.0, 0.0);
		}

		entities
			.into_iter()
			.map(|entity| {
				let (transform, render, mesh) = entity.1;

				let model_matrix = Mat4::identity().translate(transform.x, transform.y, 0.0);

				unsafe {
					let uniform_model_location = gl::GetUniformLocation(
						render.program.id,
						CString::new("uniform_model").unwrap().as_ptr(),
					);
					let uniform_view_location = gl::GetUniformLocation(
						render.program.id,
						CString::new("uniform_view").unwrap().as_ptr(),
					);
					let uniform_projection_location = gl::GetUniformLocation(
						render.program.id,
						CString::new("uniform_projection").unwrap().as_ptr(),
					);
					let uniform_color_location = gl::GetUniformLocation(
						render.program.id,
						CString::new("uniform_color").unwrap().as_ptr(),
					);

					gl::UniformMatrix4fv(
						uniform_model_location,
						1,
						gl::FALSE,
						model_matrix.flatten().as_ptr(),
					);
					gl::UniformMatrix4fv(
						uniform_view_location,
						1,
						gl::FALSE,
						view_matrix.flatten().as_ptr(),
					);
					gl::UniformMatrix4fv(
						uniform_projection_location,
						1,
						gl::FALSE,
						projection_matrix.flatten().as_ptr(),
					);
					gl::Uniform3f(
						uniform_color_location,
						mesh.color.r as f32 / 255.0,
						mesh.color.g as f32 / 255.0,
						mesh.color.b as f32 / 255.0,
					);
				}

				unsafe { gl::UseProgram(render.program.id) };
				unsafe { gl::BindVertexArray(VAO::new(mesh.vbo)) };
				unsafe {
					gl::DrawArrays(gl::TRIANGLES, 0, 6);
				}
			})
			.collect::<Vec<_>>();
	}
}

pub mod lua {
	use super::RendererSystem;
	use crate::{Entities, Mesh, Render, System, Transform};
	use luajit::ffi::luaL_Reg;
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for RendererSystem {
		fn name() -> *const i8 {
			c_str!("RendererSystem")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![lua_method!("update", RendererSystem, RendererSystem::lua_update)]
		}
	}

	pub fn new(state: &mut State) -> c_int {
		state.settop(0);
		state.push(RendererSystem {});
		1
	}

	impl RendererSystem {
		fn lua_update(&mut self, state: &mut State) -> c_int {
			let entities: &Entities = unsafe { &*state.to_userdata(2).unwrap() };
			self.update(
				&entities
					.get_entities_with_components::<(&'static Transform, &'static Render, &'static Mesh)>(
					),
			);
			0
		}
	}
}

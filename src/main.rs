extern crate glfw;

use glfw::{Action, Context, Key};

extern crate mega;

use mega::actor;
use mega::window;
use mega::Actor;
use mega::ActorCollisionMessage;
use mega::ActorCollisionSystem;
use mega::ActorRendererMessage;
use mega::ActorRendererSystem;
use mega::Collider;
use mega::Collision;
use mega::CollisionSystem;
use mega::Color;
use mega::Entities;
use mega::EventHandler;
use mega::Mesh;
use mega::Render;
use mega::RendererSystem;
use mega::Spawn;
use mega::Square;
use mega::System;
use mega::Transform;

use std::sync::mpsc::channel;
use std::sync::mpsc::{Receiver, Sender};

use std::any::Any;
use std::cell::RefCell;
use std::fmt::Debug;
use std::marker::Send;
use std::sync::{Arc, Mutex, Once, ONCE_INIT};
use std::time::Duration;
use std::{mem, thread};

#[derive(Debug, Clone)]
struct SingletonReader {
	inner: Arc<Mutex<Entities>>,
}

unsafe impl Send for SingletonReader {}

fn singleton() -> SingletonReader {
	static mut SINGLETON: *const SingletonReader = 0 as *const SingletonReader;
	static ONCE: Once = ONCE_INIT;

	unsafe {
		ONCE.call_once(|| {
			let singleton = SingletonReader {
				inner: Arc::new(Mutex::new(Entities::new())),
			};

			SINGLETON = mem::transmute(Box::new(singleton));
		});

		(*SINGLETON).clone()
	}
}

fn main() {
	//with_ecs();
	with_actor();

	println!("Mega Engine");
}

fn with_actor() {
	let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

	glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
	glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));
	glfw.window_hint(glfw::WindowHint::OpenGlProfile(glfw::OpenGlProfileHint::Core));

	let (mut window, events) =
		glfw.create_window(640, 640, "Mega Engine", glfw::WindowMode::Windowed).expect("");

	window.set_key_polling(true);
	window.make_current();
	glfw.set_swap_interval(glfw::SwapInterval::Sync(0));

	gl::load_with(|system| window.get_proc_address(system));

	let entity_manager_singleton = singleton();

	let snake = entity_manager_singleton.inner.lock().unwrap().create_entity();
	entity_manager_singleton.inner.lock().unwrap().set_component(snake, Transform::new(0.0, 0.0));
	entity_manager_singleton.inner.lock().unwrap().set_component(snake, Render::new());
	entity_manager_singleton
		.inner
		.lock()
		.unwrap()
		.set_component(snake, Mesh::new(Square::new(), Color::new(196, 80, 30)));
	entity_manager_singleton.inner.lock().unwrap().set_component(snake, Collider::new());

	let food = entity_manager_singleton.inner.lock().unwrap().create_entity();
	//entity_manager_singleton.inner.lock().unwrap().set_component(food, Transform::new(2.0, 2.0));
	entity_manager_singleton.inner.lock().unwrap().set_component(food, Transform::new(0.5, 0.5));
	entity_manager_singleton.inner.lock().unwrap().set_component(food, Render::new());
	entity_manager_singleton
		.inner
		.lock()
		.unwrap()
		.set_component(food, Mesh::new(Square::new(), Color::new(20, 100, 30)));
	entity_manager_singleton.inner.lock().unwrap().set_component(food, Collider::new());

	let enemy = entity_manager_singleton.inner.lock().unwrap().create_entity();
	entity_manager_singleton.inner.lock().unwrap().set_component(enemy, Transform::new(5.0, 2.0));
	entity_manager_singleton.inner.lock().unwrap().set_component(enemy, Render::new());
	entity_manager_singleton
		.inner
		.lock()
		.unwrap()
		.set_component(enemy, Mesh::new(Square::new(), Color::new(50, 210, 170)));
	entity_manager_singleton.inner.lock().unwrap().set_component(enemy, Collider::new());

	let friend = entity_manager_singleton.inner.lock().unwrap().create_entity();
	entity_manager_singleton.inner.lock().unwrap().set_component(friend, Transform::new(9.0, 9.0));
	entity_manager_singleton.inner.lock().unwrap().set_component(friend, Render::new());
	entity_manager_singleton
		.inner
		.lock()
		.unwrap()
		.set_component(friend, Mesh::new(Square::new(), Color::new(1, 50, 60)));
	entity_manager_singleton.inner.lock().unwrap().set_component(friend, Collider::new());

	// snake components
	let snake_transform =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Transform>(snake).unwrap();
	let snake_render =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Render>(snake).unwrap();
	let snake_mesh =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Mesh>(snake).unwrap();
	let snake_collider =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Collider>(snake).unwrap();

	let snake_renderer_components = (snake_transform, snake_render, snake_mesh);
	let snake_collider_components = (snake_transform, snake_collider, snake_mesh);

	// food components
	let food_transform =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Transform>(food).unwrap();
	let food_render =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Render>(food).unwrap();
	let food_mesh =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Mesh>(food).unwrap();
	let food_collider =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Collider>(food).unwrap();

	let food_renderer_components = (food_transform, food_render, food_mesh);
	let food_collider_components = (food_transform, food_collider, food_mesh);

	// enemy components
	let enemy_transform =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Transform>(enemy).unwrap();
	let enemy_render =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Render>(enemy).unwrap();
	let enemy_mesh =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Mesh>(enemy).unwrap();
	let enemy_collider =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Collider>(enemy).unwrap();

	let enemy_renderer_components = (enemy_transform, enemy_render, enemy_mesh);

	// friend components
	let friend_transform =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Transform>(friend).unwrap();
	let friend_render =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Render>(friend).unwrap();
	let friend_mesh =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Mesh>(friend).unwrap();
	let friend_collider =
		*entity_manager_singleton.inner.lock().unwrap().get_component::<Collider>(friend).unwrap();

	let friend_renderer_components = (friend_transform, friend_render, friend_mesh);
	//let food_collider_components = (food_transform, food_collider, food_mesh);

	// actor systems
	let mut actor_render_system = Arc::new(Mutex::new(ActorRendererSystem {
		render_context: window.render_context(),
	}));

	let actor_collision_system = Arc::new(Mutex::new(ActorCollisionSystem::new()));

	let snake_food_collision_msg = ActorCollisionMessage::Check((
		snake_collider_components.0, // snake transform
		snake_collider_components.1, // snake collider
		snake_collider_components.2, // snake mesh
		food_collider_components.0,  // food transform
		food_collider_components.1,  // food collider
		food_collider_components.2,  // food mesh
	));

	unsafe { gl::ClearColor(0.0, 0.0, 0.0, 0.0) };

	// handle collisions
	let snake_actor_collision = Spawn::spawn_with_handle(Arc::clone(&actor_collision_system));
	{
		// setup collision callback
		let callback =
			EventHandler::Rust(Arc::new(Mutex::new(RefCell::new(Box::new(handle_collision)))));
		let message = ActorCollisionMessage::Add((snake_collider, food_collider), callback);
		snake_actor_collision.tx_info.send(actor::Message::Info(message));
	}

	while !window.should_close() {
		glfw.poll_events();

		for (_, event) in glfw::flush_messages(&events) {
			match event {
				glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
					window.set_should_close(true)
				}
				_ => {}
			}
		}

		snake_actor_collision.tx_info.send(actor::Message::Info(snake_food_collision_msg.clone()));

		window.render_context().make_current();
		unsafe { gl::Clear(gl::COLOR_BUFFER_BIT) };

		render_actor(&actor_render_system, snake_renderer_components);
		render_actor(&actor_render_system, food_renderer_components);
		render_actor(&actor_render_system, enemy_renderer_components);
		render_actor(&actor_render_system, friend_renderer_components);

		window.render_context().swap_buffers();
	}

	snake_actor_collision.tx_info.send(actor::Message::Stop(false));
	snake_actor_collision.handle.join();
}

fn handle_collision() {
	println!("collision handled");
}

fn render_actor(system: &Arc<Mutex<ActorRendererSystem>>, components: (Transform, Render, Mesh)) {
	let (transform, render, mesh) = components;
	let msg = ActorRendererMessage::Render(transform, render, mesh);
	let render_actor = Spawn::spawn_with_handle(Arc::clone(system));
	if let Err(_) = render_actor.tx_info.send(actor::Message::Info(msg)) {
		panic!("error rendering actor - couldn't send render message");
	}
	if let Err(_) = render_actor.tx_info.send(actor::Message::Stop(false)) {
		panic!("error rendering actor - couldn't send stop message");
	}
	if let Err(_) = render_actor.handle.join() {
		panic!("error rendering actor - couldn't join handle");
	}
}

fn with_ecs() {
	let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
	glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
	glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));
	glfw.window_hint(glfw::WindowHint::OpenGlProfile(glfw::OpenGlProfileHint::Core));

	let (mut window, events) =
		glfw.create_window(640, 640, "Mega Engine", glfw::WindowMode::Windowed).expect("");

	window.set_key_polling(true);
	window.make_current();
	glfw.set_swap_interval(glfw::SwapInterval::Sync(0));

	gl::load_with(|system| window.get_proc_address(system));

	let mut entity_manager = Entities::new();

	let snake = entity_manager.create_entity();
	entity_manager.set_component(snake, Transform::new(0.0, 0.0));
	entity_manager.set_component(snake, Render::new());
	entity_manager.set_component(snake, Mesh::new(Square::new(), Color::new(196, 80, 30)));
	entity_manager.set_component(snake, Collider::new());

	let food = entity_manager.create_entity();
	entity_manager.set_component(food, Transform::new(2.0, 2.0));
	entity_manager.set_component(food, Render::new());
	entity_manager.set_component(food, Mesh::new(Square::new(), Color::new(20, 100, 30)));
	entity_manager.set_component(food, Collider::new());

	let enemy = entity_manager.create_entity();
	entity_manager.set_component(enemy, Transform::new(5.0, 2.0));
	entity_manager.set_component(enemy, Render::new());
	entity_manager.set_component(enemy, Mesh::new(Square::new(), Color::new(50, 210, 170)));
	entity_manager.set_component(enemy, Collider::new());

	let friend = entity_manager.create_entity();
	entity_manager.set_component(friend, Transform::new(9.0, 9.0));
	entity_manager.set_component(friend, Render::new());
	entity_manager.set_component(friend, Mesh::new(Square::new(), Color::new(1, 50, 60)));
	entity_manager.set_component(friend, Collider::new());

	let mut render_system = RendererSystem;
	let mut collision_system = CollisionSystem::new();

	while !window.should_close() {
		glfw.poll_events();

		for (_, event) in glfw::flush_messages(&events) {
			match event {
				glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
					window.set_should_close(true)
				}
				_ => {}
			}
		}

		render_system.update(
			&entity_manager
				.get_entities_with_components::<(&'static Transform, &'static Render, &'static Mesh)>(
				),
		);

		collision_system.update(&entity_manager.get_entities_with_components::<(
			&'static Transform,
			&'static Collider,
			&'static Mesh,
		)>());

		window.render_context().swap_buffers();
	}
}

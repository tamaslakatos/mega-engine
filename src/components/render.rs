use crate::ecs::component::SetComponent;
use crate::opengl::program::Program;
use crate::opengl::shader::Shader;
use std::ffi::CString;
use std::fmt::{Debug, Formatter, Result};

#[derive(Debug, Clone, Copy)]
pub struct Render {
	pub program: Program,
}

impl SetComponent for Render {}

impl Render {
	pub fn new() -> Render {
		let vertex_shaders =
			Shader::new(&CString::new(include_str!("standard.vert")).unwrap(), gl::VERTEX_SHADER);
		let fragment_shaders =
			Shader::new(&CString::new(include_str!("standard.frag")).unwrap(), gl::FRAGMENT_SHADER);
		let program = Program::new(&[vertex_shaders, fragment_shaders]);

		Render {
			program: program,
		}
	}
}

pub mod lua {
	use super::Render;
	use crate::wrapper::cell::Cell;
	use luajit::ffi::luaL_Reg;
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for Cell<Render> {
		fn name() -> *const i8 {
			c_str!("Render")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}

	pub fn new(state: &mut State) -> c_int {
		state.settop(0);
		state.push(Cell::new(Render::new()));
		1
	}
}

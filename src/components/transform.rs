use crate::ecs::component::SetComponent;

#[derive(Debug, Clone, Copy)]
pub struct Transform {
	pub x: f32,
	pub y: f32,
}

impl SetComponent for Transform {}

impl Transform {
	pub fn new(x: f32, y: f32) -> Transform {
		Transform {
			x: x,
			y: y,
		}
	}
}

pub mod lua {
	use super::Transform;
	use luajit::ffi::luaL_Reg;
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for Transform {
		fn name() -> *const i8 {
			c_str!("Transform")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![lua_method!("table", Transform, Transform::lua_to_table)]
		}
	}

	pub fn new(state: &mut State) -> c_int {
		let x = state.to_float(1).unwrap();
		let y = state.to_float(2).unwrap();
		state.settop(0);
		state.push(Transform {
			x,
			y,
		});
		1
	}

	impl Transform {
		fn lua_to_table(&self, state: &mut State) -> c_int {
			state.settop(0);
			state.new_table();
			state.push(self.x);
			state.set_field(1, "x");
			state.push(self.y);
			state.set_field(1, "y");
			state.settop(1);
			1
		}
	}
	/*
	impl LuaObject for Cell<Transform> {
		fn name() -> *const i8 {
			c_str!("Transform")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}

	pub fn new(state: &mut State) -> c_int {
		let x = state.to_float(1).unwrap();
		let y = state.to_float(2).unwrap();
		state.settop(0);
		state.push(Cell::new(Transform {
			x,
			y,
		}));
		1
	}
	*/
}

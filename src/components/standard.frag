#version 330 core

uniform vec3 uniform_color;

out vec4 color;

void main()
{
	color = vec4(uniform_color, 1.0f);
}

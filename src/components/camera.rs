
use crate::vec::vec3::Vec3;
use crate::mat::mat4::Mat4;

use std::f32::consts::PI;

#[derive(Debug, Copy, Clone)] pub struct Camera {}

impl Camera {

	fn radians(degrees: f32) -> (f32) {
		degrees * PI / 180.0
	}

	pub fn look_at(eye: Vec3, at: Vec3, up: Vec3) -> Mat4 {

		let mut mat4 = Mat4::identity();
		
		if eye == at { return mat4 }

		let mut v = (at - eye).normalize();
		let n = v.cross(up).normalize();
		let u = n.cross(v).normalize();

		v = v.negate();

		mat4.arr[0][0] = n.x;
		mat4.arr[0][1] = n.y;
		mat4.arr[0][2] = n.z;
		mat4.arr[0][3] = -n.dot(eye);
		mat4.arr[1][0] = u.x;
		mat4.arr[1][1] = u.y;
		mat4.arr[1][2] = u.z;
		mat4.arr[1][3] = -u.dot(eye);
		mat4.arr[2][0] = v.x;
		mat4.arr[2][1] = v.y;
		mat4.arr[2][2] = v.z;
		mat4.arr[2][3] = -v.dot(eye);

		mat4
	}

	pub fn ortho(left: f32, right: f32, bottom: f32, top: f32, 
		near: f32, far: f32) -> Mat4 {

		if left == right { panic!("ERROR: Left And Right Are Equal."); }
		if bottom == top { panic!("ERROR: Bottom And Top Are Equal."); }
		if near == far   { panic!("ERROR: Near And Far Are Equal."); }

		let w = right - left;
		let h = top - bottom;
		let d = far - near;

		let mut mat4 = Mat4::identity();
		
		mat4.arr[0][0] = 2.0 / w;
		mat4.arr[1][1] = 2.0 / h;
		mat4.arr[2][2] = -2.0 / d;
		mat4.arr[0][3] = -(left + right) / w;
		mat4.arr[1][3] = -(top + bottom) / h;
		mat4.arr[2][3] = -(near + far) / d;

		mat4
	}

	pub fn perspective(fovy: f32, aspect: f32, 
		near: f32, far: f32) -> Mat4 {

		let f = 1.0 / ( Camera::radians(fovy) / 2.0 ).tan();
		let d = far - near;

		let mut mat4 = Mat4::identity();

		mat4.arr[0][0] = f / aspect;
		mat4.arr[1][1] = f;
		mat4.arr[2][2] = -(near + far) / d;
		mat4.arr[2][3] = -2.0 * near * far / d;
		mat4.arr[3][2] = -1.0;
		mat4.arr[3][3] = 0.0;

		mat4
	}
}
use crate::ecs::component::SetComponent;
use crate::opengl::vbo::VBO;
use crate::shapes::shape::Shape;
use crate::vec::vec2::Vec2;

use std::any::Any;

#[derive(Debug, Clone, Copy)]
pub struct Mesh {
	//pub shape: Box<Shape>, - AUGUST
	pub vert_min: Vec2,
	pub vert_max: Vec2,
	pub vbo: u32,
	pub color: Color,
}

impl SetComponent for Mesh {}

#[derive(Debug, Clone, Copy)]
pub struct Color {
	pub r: u8,
	pub g: u8,
	pub b: u8,
}

impl Mesh {
	pub fn new<T: Any + Shape>(shape: T, color: Color) -> Mesh {
		let vbo = VBO::new(shape.vert());

		let (min, max) = shape
			.vert()
			.iter()
			.fold((shape.vert()[0], shape.vert()[0]), |acc, &x| (acc.0.min(x), acc.1.max(x)));

		//println!("mesh bounds: {}, {}", min, max);
		//println!("mesh color: {}, {}, {}", color.r, color.g, color.b);

		Mesh {
			//shape: Box::new(shape), - AUGUST
			vert_min: Vec2::init(min, min),
			vert_max: Vec2::init(max, max),
			vbo: vbo,
			color: color,
		}
	}
}

impl Color {
	pub fn new(r: u8, g: u8, b: u8) -> Color {
		Color {
			r: r,
			g: g,
			b: b,
		}
	}
}

pub mod lua {
	use super::{Color, Mesh};
	use crate::Square;
	use luajit::ffi::luaL_Reg;
	use luajit::{c_int, LuaObject, State};

	pub fn setup_table(state: &mut State) {
		state.new_table();
		state.register_fns(None, vec![lua_func!("square", new_square_mesh)]);
	}

	impl LuaObject for Mesh {
		fn name() -> *const i8 {
			c_str!("Mesh")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}

	pub fn new_square_mesh(state: &mut State) -> c_int {
		let square = Square::new();
		state.get_field(1, "r");
		state.get_field(1, "g");
		state.get_field(1, "b");
		let color = Color::new(
			state.to_int(-3).unwrap() as u8,
			state.to_int(-2).unwrap() as u8,
			state.to_int(-1).unwrap() as u8,
		);

		state.settop(0);
		state.push(Mesh::new(square, color));
		//state.push(Cell::new(Mesh::new(square, color)));
		1
	}
}

use crate::ecs::component::SetComponent;

#[derive(Debug, Clone, Copy)]
pub struct Velocity {
	pub x: f32,
	pub y: f32,
}

impl SetComponent for Velocity {}

impl Velocity {
	pub fn new(x: f32, y: f32) -> Velocity {
		Velocity {
			x: x,
			y: y,
		}
	}
}

pub mod lua {
	use super::Velocity;
	use crate::wrapper::cell::Cell;
	use luajit::ffi::luaL_Reg;
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for Cell<Velocity> {
		fn name() -> *const i8 {
			c_str!("Velocity")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}

	pub fn new(state: &mut State) -> c_int {
		let x = state.to_float(1).unwrap();
		let y = state.to_float(2).unwrap();
		state.settop(0);
		state.push(Cell::new(Velocity {
			x,
			y,
		}));
		1
	}
}

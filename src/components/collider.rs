#![feature(integer_atomics)]
use crate::ecs::component::SetComponent;
use crate::vec::vec2::Vec2;
use std::hash::{Hash, Hasher};
use std::sync::atomic::{AtomicU64, Ordering};

#[derive(Debug, Clone, Copy, Eq)]
pub struct Collider {
	pub id: u64,
	pub min: Vec2,
	pub max: Vec2,
}

impl SetComponent for Collider {}

impl Collider {
	pub fn new() -> Collider {
		static next_id: AtomicU64 = AtomicU64::new(0);
		Collider {
			id: next_id.fetch_add(1, Ordering::Relaxed),
			min: Vec2::init_empty(),
			max: Vec2::init_empty(),
		}
	}

	pub fn set(&self, id: u64, min: Vec2, max: Vec2) -> Collider {
		Collider {
			id, // TODO get from self
			min,
			max,
		}
	}
}

impl<'a> PartialEq for Collider {
	fn eq(&self, rhs: &Collider) -> bool {
		self.id == rhs.id
	}
}

impl Hash for Collider {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.id.hash(state);
	}
}

#[derive(Debug)]
pub struct Collision<'a> {
	pub a: &'a Collider,
	pub b: &'a Collider,
}

impl<'a> Collision<'a> {
	pub fn new(a: &'a Collider, b: &'a Collider) -> Collision<'a> {
		Collision {
			a,
			b,
		}
	}
}

impl<'a> PartialEq for Collision<'a> {
	fn eq(&self, rhs: &Collision) -> bool {
		self.a == rhs.a && self.b == rhs.b
	}
}

impl<'a> Eq for Collision<'a> {}
impl<'a> Hash for Collision<'a> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.a.hash(state);
		self.b.hash(state);
	}
}

pub mod lua {
	use super::{Collider, Collision};
	use luajit::ffi::luaL_Reg;
	use luajit::{c_int, LuaObject, State};

	impl LuaObject for Collider {
		fn name() -> *const i8 {
			c_str!("Collider")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}

	pub fn new(state: &mut State) -> c_int {
		state.settop(0);
		state.push(Collider::new());
		1
	}

	impl<'a> LuaObject for Collision<'a> {
		fn name() -> *const i8 {
			c_str!("ColliderPair")
		}

		fn lua_fns() -> Vec<luaL_Reg> {
			vec![]
		}
	}
}

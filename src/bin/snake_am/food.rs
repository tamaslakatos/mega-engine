use mega::{Collider, Color, Mesh, Render, Square, Transform};

#[derive(Clone, Copy)]
pub struct Food {
	pub position: Transform,
	pub render: Render,
	pub mesh: Mesh,
	pub collider: Collider,
}

impl Food {
	pub fn new(xpos: f32, ypos: f32) -> Self {
		Self {
			position: Transform::new(xpos, ypos),
			mesh: Mesh::new(Square::new(), Color::new(0xff, 0x00, 0x00)),
			render: Render::new(),
			collider: Collider::new(),
		}
	}
}

use mega::{Collider, Color, Mesh, Render, Square, Transform, Vec2};

pub struct Snake {
	pub position: Transform,
	pub mesh: Mesh,
	pub render: Render,
	pub collider: Collider,
	pub velocity: Vec2,
}

impl Snake {
	pub fn new(xpos: f32, ypos: f32, xdir: f32, ydir: f32) -> Self {
		Self {
			position: Transform::new(xpos, ypos),
			mesh: Mesh::new(Square::new(), Color::new(0x00, 0xff, 0x00)),
			render: Render::new(),
			collider: Collider::new(),
			velocity: Vec2::init(xdir, ydir),
		}
	}

	pub fn update(&mut self) {
		let new_x = self.position.x + self.velocity.x;
		if new_x < 0.0 {
			self.position.x = 0.0;
			self.velocity.x = 0.0;
		} else if new_x > 9.0 {
			self.position.x = 9.0;
			self.velocity.x = 0.0;
		} else {
			self.position.x = new_x;
		}

		let new_y = self.position.y + self.velocity.y;
		if new_y < 0.0 {
			self.position.y = 0.0;
			self.velocity.y = 0.0;
		} else if new_y > 9.0 {
			self.position.y = 9.0;
			self.velocity.y = 0.0;
		} else {
			self.position.y = new_y;
		}

		self.position = Transform::new(new_x, new_y);
	}

	pub fn turn(&mut self, xdir: f32, ydir: f32) {
		self.velocity.x = xdir;
		self.velocity.y = ydir;
	}
}

use crate::food::Food;
use crate::snake::Snake;

use mega::actor;
use mega::window;
use mega::Actor;
use mega::ActorCollisionMessage;
use mega::ActorCollisionSystem;
use mega::ActorRendererMessage;
use mega::ActorRendererSystem;
use mega::EventHandler;
use mega::Spawn;

use rand::prelude::*;
use std::cell::RefCell;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};

mod food;
mod snake;

const FPS_SAMPLE_RATE_SECS: u128 = 1;

pub fn main() {
	window::init();

	// game objects
	let snake = Arc::new(Mutex::new(Snake::new(4.5, 2.0, 0.0, 0.001)));
	let food = Arc::new(Mutex::new(Food::new(4.5, 7.0)));

	// snake controls
	set_snake_turn_callback(&snake, glfw::Key::W, 0.000, 0.001, "snake goes up".to_string());
	set_snake_turn_callback(&snake, glfw::Key::A, -0.001, 0.000, "snake goes left".to_string());
	set_snake_turn_callback(&snake, glfw::Key::S, 0.000, -0.001, "snake goes down".to_string());
	set_snake_turn_callback(&snake, glfw::Key::D, 0.001, 0.000, "snake goes right".to_string());

	// actor systems
	let renderer_actor = Spawn::spawn_with_handle(Arc::new(Mutex::new(ActorRendererSystem::new())));
	let collision_actor =
		Spawn::spawn_with_handle(Arc::new(Mutex::new(ActorCollisionSystem::new())));

	// collision handler
	{
		// create vars to be moved into new thread
		let snake_collider = snake.lock().unwrap().collider;
		let food_collider = food.lock().unwrap().collider;
		let food = Arc::clone(&food);
		let mut score = 0;

		let collision_handler = EventHandler::new(move || {
			let mut food = food.lock().unwrap();
			food.position.x = random::<f32>() * 9.0;
			food.position.y = random::<f32>() * 9.0;
			score += 1;
			println!("You have {} points!", score);
		});

		collision_actor.tx_info.send(actor::Message::Info(ActorCollisionMessage::Add(
			(snake_collider, food_collider),
			collision_handler,
		)));
	}

	// game loop
	let mut frame_counter = 0;
	let fps_sample_delay = Duration::from_secs(FPS_SAMPLE_RATE_SECS as u64);
	let mut last_fps_sample_time = Instant::now();
	let mut next_fps_sample_time = last_fps_sample_time + fps_sample_delay;
	while !window::should_close() {
		frame_counter += 1;

		window::clear();
		window::handle_events();

		// check & handle collisions
		{
			let (sp, sc, sm) = {
				let snake = snake.lock().unwrap();
				(snake.position, snake.collider, snake.mesh)
			};
			let (fp, fc, fm) = {
				let food = food.lock().unwrap();
				(food.position, food.collider, food.mesh)
			};
			collision_actor
				.tx_info
				.send(actor::Message::Info(ActorCollisionMessage::Check((sp, sc, sm, fp, fc, fm))));
		}

		// move & render snake
		{
			let mut snake = snake.lock().unwrap();
			snake.update();
			renderer_actor.tx_info.send(actor::Message::Info(ActorRendererMessage::Render(
				snake.position,
				snake.render,
				snake.mesh,
			)));
			renderer_actor.tx_info.send(actor::Message::Ping);
			renderer_actor.rx_sync.recv();
		}

		// render food
		{
			let mut food = food.lock().unwrap();
			renderer_actor.tx_info.send(actor::Message::Info(ActorRendererMessage::Render(
				food.position,
				food.render,
				food.mesh,
			)));
			renderer_actor.tx_info.send(actor::Message::Ping);
			renderer_actor.rx_sync.recv();
		}

		window::swap_buffers();

		// sample FPS at regular intervals
		let frame_end_time = Instant::now();
		if frame_end_time >= next_fps_sample_time {
			let duration_ns = frame_end_time.duration_since(last_fps_sample_time).as_nanos();
			let fps = frame_counter as f64 / (duration_ns as f64 / 1000000000.0);
			println!("FPS: {}", fps);
			last_fps_sample_time = frame_end_time;
			next_fps_sample_time += fps_sample_delay;
			frame_counter = 0;
		}
	}
}

fn set_snake_turn_callback(
	snake: &Arc<Mutex<Snake>>,
	key: glfw::Key,
	x: f32,
	y: f32,
	text: String,
) {
	let snake = Arc::clone(snake);
	window::set_keypress_callback(
		key,
		Arc::new(Mutex::new(RefCell::new(Box::new(move || {
			//println!("{}", text);
			snake.lock().unwrap().turn(x, y);
		})))),
	);
}

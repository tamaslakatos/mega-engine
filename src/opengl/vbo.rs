
extern crate gl;

use gl::types::{ GLuint, GLsizeiptr, GLvoid };

use std::mem::size_of;

#[derive(Debug)] pub struct VBO {}

impl VBO {

	pub fn new(vert: &[f32]) -> GLuint {

		unsafe {

			let mut vbo: GLuint = 0;

			gl::GenBuffers(1, &mut vbo); gl::BindBuffer(gl::ARRAY_BUFFER, vbo);

			gl::BufferData(
				gl::ARRAY_BUFFER,
				(vert.len() * size_of::<f32>()) as GLsizeiptr,
				vert.as_ptr() as *const GLvoid,
				gl::STATIC_DRAW,
			);

			gl::BindBuffer(gl::ARRAY_BUFFER, 0);

			vbo
		}
	}
}
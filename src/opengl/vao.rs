
extern crate gl;

use gl::types::{ GLuint, GLint };

use std::mem::size_of;
use std::ptr::null;

const ATTRIB_POSITION_OFFSET: usize = 2;
const ATTRIB_POSITION_SIZE: i32 = 2;

#[derive(Debug)] pub struct VAO {}

impl VAO {

	pub fn new(vbo: GLuint) -> GLuint {

		unsafe {

			let mut vao: GLuint = 0;

			gl::GenVertexArrays(1, &mut vao); gl::BindVertexArray(vao);

			gl::BindBuffer(gl::ARRAY_BUFFER, vbo);

			gl::EnableVertexAttribArray(0);
			gl::VertexAttribPointer(0, ATTRIB_POSITION_SIZE, gl::FLOAT, gl::FALSE, (ATTRIB_POSITION_OFFSET * size_of::<f32>()) as GLint, null());

			gl::BindBuffer(gl::ARRAY_BUFFER, 0);
			gl::BindVertexArray(0);

			vao
		}
	}
}
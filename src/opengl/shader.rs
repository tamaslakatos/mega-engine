extern crate gl;

use gl::types::{GLchar, GLenum, GLint, GLuint};
use std::ffi::CString;

use std::ffi::CStr;
use std::ptr::null;

#[derive(Debug, Clone, Copy)]
pub struct Shader {
	pub id: GLuint,
}

impl Shader {
	pub fn new(shader_source: &CStr, shader_type: GLenum) -> Shader {
		let id: GLuint = unsafe { gl::CreateShader(shader_type) };

		unsafe {
			gl::ShaderSource(id, 1, &shader_source.as_ptr(), null());
		}
		unsafe { gl::CompileShader(id) }

		// handle errors
		let mut status = gl::FALSE as GLint;
		unsafe {
			gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut status);
		}

		if status != (gl::TRUE as GLint) {
			println!("couldn't compile shader");
			println!("error msg: {}", Self::get_compile_err_msg(id));
		}
		Shader {
			id: id,
		}
	}

	fn get_compile_err_msg(id: GLuint) -> String {
		let mut len = 0;
		unsafe {
			gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
		}
		if len == 0 {
			return String::from("no error message");
		}

		let mut errmsg = Vec::with_capacity(len as usize + 1);
		errmsg.extend([b' '].iter().cycle().take(len as usize));
		let errmsg = unsafe { CString::from_vec_unchecked(errmsg) };
		unsafe {
			gl::GetShaderInfoLog(id, len, std::ptr::null_mut(), errmsg.as_ptr() as *mut GLchar);
		}
		errmsg.to_string_lossy().into_owned()
	}
}

extern crate gl;

use gl::types::{GLchar, GLint, GLuint};
use std::ffi::CString;

use crate::opengl::shader::Shader;

#[derive(Debug, Clone, Copy)]
pub struct Program {
	pub id: GLuint,
}

impl Program {
	pub fn new(shaders: &[Shader]) -> Program {
		let id: GLuint = unsafe { gl::CreateProgram() };

		for shader in shaders {
			unsafe { gl::AttachShader(id, shader.id) };
		}

		unsafe { gl::LinkProgram(id) }

		for shader in shaders {
			unsafe { gl::DetachShader(id, shader.id) };
		}

		// handle errors
		let mut status = gl::FALSE as GLint;
		unsafe {
			gl::GetProgramiv(id, gl::LINK_STATUS, &mut status);
		}

		if status != (gl::TRUE as GLint) {
			println!("couldn't link shader");
			println!("error msg: {}", Self::get_link_err_msg(id));
		}
		Program {
			id: id,
		}
	}

	fn get_link_err_msg(id: GLuint) -> String {
		let mut len = 0;
		unsafe {
			gl::GetProgramiv(id, gl::INFO_LOG_LENGTH, &mut len);
		}
		if len == 0 {
			return String::from("no error message");
		}

		let mut errmsg = Vec::with_capacity(len as usize + 1);
		errmsg.extend([b' '].iter().cycle().take(len as usize));
		let errmsg = unsafe { CString::from_vec_unchecked(errmsg) };
		unsafe {
			gl::GetProgramInfoLog(id, len, std::ptr::null_mut(), errmsg.as_ptr() as *mut GLchar);
		}
		errmsg.to_string_lossy().into_owned()
	}
}

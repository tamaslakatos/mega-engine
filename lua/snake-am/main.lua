mega = require("libmega")
math.randomseed(os.time())

mega.window.start()

-- direction velocities
local UP    = {x =  0.0  , y =  0.001}
local RIGHT = {x =  0.001, y =  0.0  }
local DOWN  = {x =  0.0  , y = -0.001}
local LEFT  = {x = -0.001, y =  0.0  }
local STOP  = {x =  0.0  , y =  0.0  }

-- colors
local RED   = {r = 0xff, g = 0x00, b = 0x00}
local GREEN = {r = 0x00, g = 0xff, b = 0x00}

local entities = mega.entities()
print("lua - entities created")

-- snake
local snake = {
	entity = entities:create_entity(),
	position = mega.transform(4.5, 2.0),
	render = mega.render(),
	shape = mega.square(),
	color = GREEN,
	collider = mega.collider(),
	velocity = UP,
	score = 0,
}

entities:set_transform(snake.entity, snake.position)
entities:set_square_mesh(snake.entity, snake.shape, snake.color)
entities:set_collider(snake.entity, snake.collider)
entities:set_render(snake.entity, snake.render)

print("snake created")

function snake:move()
	local snake = self.entity;
	local pos = entities:get_transform(snake):table()
	if (pos.x < 0.0) then
		entities:set_transform(snake, mega.transform(
			0.0, pos.y
		))
		self.velocity = STOP
	elseif (pos.x > 9.0) then
		entities:set_transform(snake, mega.transform(
			9.0, pos.y
		))
		self.velocity = STOP
	elseif (pos.y < 0.0) then
		entities:set_transform(snake, mega.transform(
			pos.x, 0.0
		))
		self.velocity = STOP
	elseif (pos.y > 9.0) then
		entities:set_transform(snake, mega.transform(
			pos.x, 9.0
		))
		self.velocity = STOP
	else
		local vel = self.velocity
		entities:set_transform(snake, mega.transform(
			pos.x + vel.x,
			pos.y + vel.y
		))
	end
end

function snake:turn_up()
	self.velocity = UP
end
function turn_up()
	--print("turn up")
	snake:turn_up()
end
function snake:turn_right()
	self.velocity = RIGHT
end
function turn_right()
	--print("turn right")
	snake:turn_right()
end
function snake:turn_down()
	self.velocity = DOWN
end
function turn_down()
	--print("turn down")
	snake:turn_down()
end
function snake:turn_left()
	self.velocity = LEFT
end
function turn_left()
	--print("turn left")
	snake:turn_left()
end

-- food
local food = {
	entity = entities:create_entity(),
	position = mega.transform(4.5, 7.0),
	render = mega.render(),
	shape = mega.square(),
	color = RED,
	collider = mega.collider(),
}
entities:set_transform(food.entity, food.position)
entities:set_square_mesh(food.entity, food.shape, food.color)
entities:set_collider(food.entity, food.collider)
entities:set_render(food.entity, food.render)

print("food created")

function eat_food()
	snake.score = snake.score + 1
	print("got food! new score: " .. snake.score)

	local x = math.random(0.0, 9.0)
	local y = math.random(0.0, 9.0)
	food.position = mega.transform(x, y)
	entities:set_transform(food.entity, food.position)

	-- TODO add snake segment
end
mega.on_keypress(mega.key.ESC, mega.window.close)
mega.on_keypress(mega.key.W, turn_up)
mega.on_keypress(mega.key.A, turn_left)
mega.on_keypress(mega.key.S, turn_down)
mega.on_keypress(mega.key.D, turn_right)
mega.on_keypress(mega.key.F, snake_eat_food)

local framecount = 0
local time = os.clock()
print("start time: " .. time)

local rendering_actor = mega.actor.rendering()

while true do
	framecount = framecount + 1

	mega.handle_events()
	if mega.window.should_close() then
		print("game over")
		break
	end
	snake:move()

	rendering_actor:render(snake.position, snake.render, snake.mesh)
	rendering_actor:render(food.position, food.render, food.mesh)
	mega.window.swap_buffers()

	local newtime = os.clock()
	local timediff = newtime - time
	if timediff >= 1.0 then
		time = newtime
		print("FPS: ".. framecount / timediff)
		framecount = 0
	end
end

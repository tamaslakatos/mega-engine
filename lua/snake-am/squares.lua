mega = require("libmega")
mega.window.start()

local RED   = {r = 0xff, g = 0x00, b = 0x00}
local GREEN = {r = 0x00, g = 0xff, b = 0x00}

local entities = mega.entities()

local square1 = {
	entity = entities:create_entity(),
	position = mega.transform(0.0, 0.0),
	render = mega.render(),
	shape = mega.square(),
	color = RED,
}

local square2 = {
	entity = entities:create_entity(),
	position = mega.transform(3.0, 2.0),
	render = mega.render(),
	shape = mega.square(),
	color = GREEN,
}

entities:set_transform(square1.entity, square1.position)
entities:set_square_mesh(square1.entity, square1.shape, square1.color)
entities:set_render(square1.entity, square1.render)

entities:set_transform(square1.entity, square1.position)
entities:set_square_mesh(square2.entity, square2.shape, square2.color)
entities:set_render(square2.entity, square2.render)

local renderer = mega.system.renderer()

print("entering game loop")
while (not mega.window.should_close()) do
	renderer:update(entities)
	mega.window.swap_buffers()
end
